FROM node:20-alpine AS development-build-stage

USER node

WORKDIR /usr/src/app

COPY --chown=node package*.json ./

RUN npm install glob rimraf
RUN npm install

COPY --chown=node src/ src/
COPY --chown=node tools/jena tools/jena
COPY --chown=node tsconfig.build.json tsconfig.build.json
COPY --chown=node tsconfig.json tsconfig.json
COPY --chown=node nest-cli.json nest-cli.json

RUN BASE_URL=$BASE_URL npm run build

FROM node:20-alpine@sha256:2ffec31a58e85fbcd575c544a3584f6f4d128779e6b856153a04366b8dd01bb0 AS production-build-stage

ENV NODE_ENV=production

RUN apk update && apk add openjdk17

USER node
WORKDIR /usr/src/app


COPY --chown=node package*.json ./

RUN npm ci --only=production

COPY --from=development-build-stage /usr/src/app/dist ./dist

CMD ["node", "dist/src/main"]
