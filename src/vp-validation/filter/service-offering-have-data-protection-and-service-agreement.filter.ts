import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.1.1">P2.1.1 labelling criterion</a>.
 * <a href="https://gitlab.com/gaia-x/technical-committee/service-characteristics-working-group/service-characteristics/-/blame/develop/single-point-of-truth/legal-document.yaml?ref_type=heads#L114">DataProtectionRegulationMeasures and ServiceAgreementOffer Shacl definition</a>.
 */
export class ServiceOfferingHaveDataProtectionAndServiceAgreementFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHaveDataProtectionAndServiceAgreementFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(
      `Checking that service offerings have at least one DataProtectionRegulationMeasures and ServiceAgreementOffer legal documents for VPUUID ${vpUUID}...`
    )

    const errorMessages: string[] = ['DataProtectionRegulationMeasures', 'ServiceAgreementOffer'].flatMap(requiredDocument =>
      results
        .filter(result => !result.legalDocuments.some(legalDocument => legalDocument.type === `w3id.org/gaia-x/development#${requiredDocument}`))
        .map(result => {
          this.logger.error(
            `P2.1.1 - Service offering ${result.serviceOfferingId} is missing a ${requiredDocument} legal document for VPUUID ${vpUUID}...`
          )
          return `P2.1.1 - Service offering ${result.serviceOfferingId} is missing a ${requiredDocument} legal document`
        })
    )

    return {
      conforms: !errorMessages.length,
      results: errorMessages
    }
  }
}
