import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'
import { ValidationResult } from 'src/common/dto'

import { VerifiablePresentation } from '../../common/constants'
import { graphValueFormat } from '../../common/utils/graph-value-format'
import { TrustedNotaryIssuerService } from '../service/trusted-notary-issuer.service'
import { ValidationFilter } from './common/validation-filter.interface'

export class RegistrationNumbersAreFromTrustedIssuersFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(RegistrationNumbersAreFromTrustedIssuersFilter.name)

  constructor(private readonly trustedNotaryIssuerService: TrustedNotaryIssuerService) {}

  async doFilter(vpUUID: string, _verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Searching for registration number issuers VPUUID ${vpUUID}...`)

    const session = driver.session()
    const query = `MATCH (issuer)<-[r:_https_www_w3_org_2018_credentials_issuer_]-(node)
      -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(nodeType)
      WHERE issuer.vpID="${vpUUID}" 
        AND (
          nodeType.value=~".*LocalRegistrationNumber.*"
            OR nodeType.value=~".*VatID.*" 
            OR nodeType.value=~".*LeiCode.*" 
            OR nodeType.value=~".*EORI.*" 
            OR nodeType.value=~".*EUID.*" 
            OR nodeType.value=~".*TaxID.*"
        )
      RETURN DISTINCT issuer;`

    try {
      const results = await session.executeRead(tx => tx.run(query))
      const registrationNumberIssuers: string[] = results.records.map(record => {
        return graphValueFormat(record.get('issuer')?.properties?.value)
      })

      this.logger.debug(`Found the following registration number issuers for VPUUID ${vpUUID}: ${registrationNumberIssuers.join(', ')}`)

      const invalidIssuers: string[] = []
      for (const issuer of registrationNumberIssuers) {
        if (!(await this.trustedNotaryIssuerService.isTrusted(issuer))) {
          this.logger.error(`${issuer} is not a trusted registration number issuer for VPUUID ${vpUUID}`)
          invalidIssuers.push(issuer)
        }
      }

      return {
        conforms: invalidIssuers.length === 0,
        results: invalidIssuers.map(invalidIssuer => `${invalidIssuer} is not a trusted registration number issuer`)
      }
    } catch (error) {
      this.logger.error(`An error occurred while checking registration number trusted issuers for VPUUID ${vpUUID}. Error: ${error.message}`)

      return {
        conforms: false,
        results: [`An error occurred while checking registration number trusted issuers: ${error.message}`]
      }
    } finally {
      await session.close()
    }
  }
}
