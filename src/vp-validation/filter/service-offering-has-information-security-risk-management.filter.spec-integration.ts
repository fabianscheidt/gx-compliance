import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import vpWithInvalidServiceOfferings from '../filter/fixtures/service-offering-has-information-security-risk-management-filter/vp-with-invalid-service-offerings.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-information-security-risk-management-filter/vp-with-valid-service-offering.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasInformationSecurityRiskManagementFilter } from './service-offering-has-information-security-risk-management.filter'

describe('ServiceOfferingHasInformationSecurityRiskManagementFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasInformationSecurityRiskManagementFilter()

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a negative conformity with a reason when customer auditing rights are missing from service offerings', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithInvalidServiceOfferings)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)

      expect(result.results).toEqual([
        'P3.1.3 - Service offering <https://example.org/invalid-service-offering-2.json> is missing information security risk management in its legal documents',
        'P3.1.3 - Service offering <https://example.org/invalid-service-offering-1.json> is missing information security risk management in its legal documents'
      ])
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
    })
  })
})
