import { Logger } from '@nestjs/common'

import { Driver, Session } from 'neo4j-driver'
import { VerifiablePresentation } from 'src/common/constants'

import { ValidationResult } from '../../../common/dto'
import { LegalDocument } from '../../../common/model/legal-document'
import { LegalDocumentUtil } from '../../../common/utils/legal-document.util'
import { ValidationFilter } from './validation-filter.interface'

export interface ServiceOfferingLegalDocuments {
  serviceOfferingId: string
  legalDocuments: LegalDocument[]
}

/**
 * Specific {@link ValidationFilter} class that focuses on collecting legal
 * documents types from service offerings to then process them within the
 * {@link #verifyLegalDocuments} function.
 */
export abstract class LegalDocumentFilter implements ValidationFilter {
  protected readonly logger: Logger

  protected constructor(logger: Logger) {
    this.logger = logger
  }

  /**
   * This function must be implemented in order to process the extracted service offerings and their legal documents.
   *
   * @param vpUUID the verifiable presentation's unique identifier
   * @param results a list of {@link ServiceOfferingLegalDocuments} in which each element represents a service offering
   * and its legal documents
   */
  abstract verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult

  async doFilter(vpUUID: string, _verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Collecting legal document types for VPUUID ${vpUUID}...`)

    const query = `MATCH (serviceOffering)-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_development_ServiceOffering_)
      WHERE serviceOffering.vpID="${vpUUID}"
      WITH serviceOffering
      OPTIONAL MATCH (property)<-[r*]-(legalDocument)
        <-[:_https_w3id_org_gaia_x_development_legalDocuments_]-()
        <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-(serviceOffering)
      WITH serviceOffering.value AS serviceOfferingId, legalDocument, COLLECT({ type: type(r[0]), value: property.value }) AS legalDocumentProperties
      RETURN serviceOfferingId, legalDocument, legalDocumentProperties`

    const session: Session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      this.logger.debug(`Found ${results.records.length} legal documents in service offerings for VPUUID ${vpUUID}...`)

      const filterResults: Map<string, ServiceOfferingLegalDocuments> = new Map<string, ServiceOfferingLegalDocuments>()
      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')

        let filterResult: ServiceOfferingLegalDocuments = filterResults.get(serviceOfferingId)
        if (!filterResult) {
          filterResult = { serviceOfferingId, legalDocuments: [] }
        }

        if (record.get('legalDocument')) {
          const legalDocument: LegalDocument = LegalDocumentUtil.mapFromProperties(record.get('legalDocumentProperties'))

          try {
            LegalDocumentUtil.isValid(legalDocument)
          } catch (error) {
            errorMessages.push(`Legal document with URL <${legalDocument.url}> is invalid: ${error}`)
          }

          filterResult.legalDocuments.push(legalDocument)
        } else {
          this.logger.debug(`No legal document found for service offering ${serviceOfferingId} for VPUUID ${vpUUID}...`)
        }

        filterResults.set(serviceOfferingId, filterResult)
      }

      if (errorMessages.length) {
        return {
          conforms: false,
          results: errorMessages
        }
      }

      return this.verifyLegalDocuments(vpUUID, Array.from(filterResults.values()))
    } catch (e) {
      this.logger.error(`An unexpected error has occurred while collecting legal documents for VPUUID ${vpUUID} : ${e.message}`)

      return {
        conforms: false,
        results: [`An unexpected error has occurred while collecting legal documents: ${e.message}`]
      }
    } finally {
      await session.close()
    }
  }
}
