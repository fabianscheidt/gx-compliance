import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../../common/constants'
import { ValidationResult } from '../../../common/dto'

export interface ValidationFilter {
  doFilter(vpUUID: string, verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult>
}
