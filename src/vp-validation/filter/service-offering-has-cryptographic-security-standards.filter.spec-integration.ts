import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithInvalidServiceOfferings from './fixtures/service-offering-has-cryptographic-security-standards-filter/vp-with-invalid-service-offerings.json'
import vpWithValidServiceOffering from './fixtures/service-offering-has-cryptographic-security-standards-filter/vp-with-valid-service-offering.json'
import { ServiceOfferingHasCryptographicSecurityStandards } from './service-offering-has-cryptographic-security-standards.filter'

describe('ServiceOfferingHasCryptographicSecurityStandards', () => {
  memgraphTestSuite(driver => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasCryptographicSecurityStandards()

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a non conform result with reason when cryptographic security standards are missing', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithInvalidServiceOfferings)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        `P3.1.9 - Service offering <https://example.org/service-offering-1> is missing cryptographic security standards`,
        `P3.1.9 - Service offering <https://example.org/service-offering-2> is missing cryptographic security standards`
      ])
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An error occurred while checking that service offerings have cryptographic security standards : Test error'])
    })
  })
})
