import { Logger } from '@nestjs/common'

import { Driver, Session } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { ValidationFilter } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.1.3">P1.1.3 labelling criterion</a>.
 */
export class LegalDocumentHasInvolvedParties implements ValidationFilter {
  private readonly logger: Logger = new Logger(LegalDocumentHasInvolvedParties.name)

  async doFilter(vpUUID: string, _verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Checking that service offering legal documents have involved parties for VPUUID ${vpUUID}...`)

    const query = `MATCH (:_https_w3id_org_gaia_x_development_LegallyBindingAct_)<-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]-(legallyBindingAct)
        <-[:_https_w3id_org_gaia_x_development_legalDocuments_]-()
        <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-(serviceOffering)
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_development_ServiceOffering_)
      WHERE serviceOffering.vpID="${vpUUID}"
      CALL {
        WITH legallyBindingAct
        OPTIONAL MATCH (legallyBindingAct)-[:_https_w3id_org_gaia_x_development_involvedParties_]->(involvedParties)
        RETURN DISTINCT involvedParties
      }
      MATCH (legalPerson)<-[:_https_www_w3_org_2018_credentials_credentialSubject_]-()
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_development_LegalPerson_)
      WHERE serviceOffering.vpID="${vpUUID}"
      RETURN legalPerson.value AS legalPerson, serviceOffering.value AS serviceOfferingId, legallyBindingAct, involvedParties.value AS involvedParty`

    const session: Session = driver.session()

    try {
      const results = await session.executeRead(tx => tx.run(query))
      const legalPersonList: string[] = []
      for (const record of results.records) {
        const legalPerson: string = record.get('legalPerson')
        this.logger.debug(`Adding ${legalPerson} to valid involved parties for VPUUID ${vpUUID}...`)
        legalPersonList.push(legalPerson)
      }

      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')
        const involvedParty: string = record.get('involvedParty')

        if (!involvedParty) {
          this.logger.error(
            `P1.1.3 - Legally binding act of service offering ${serviceOfferingId} is missing involved parties for VPUUID ${vpUUID}...`
          )
          errorMessages.push(`P1.1.3 - Legally binding act of service offering ${serviceOfferingId} is missing involved parties`)
        } else if (!legalPersonList.includes(involvedParty)) {
          this.logger.error(
            `P1.1.3 - Legally binding act of service offering ${serviceOfferingId} is referencing an involved party ${involvedParty} that does not match any legal person for VPUUID ${vpUUID}...`
          )
          errorMessages.push(
            `P1.1.3 - Legally binding act of service offering ${serviceOfferingId} is referencing an involved party ${involvedParty} that does not match any legal person`
          )
        }
      }

      return {
        conforms: !errorMessages.length,
        results: errorMessages
      }
    } catch (error) {
      this.logger.error(`An error occurred while checking P1.1.3 for VPUUID ${vpUUID}. Error: ${error.message}`)

      return {
        conforms: false,
        results: [`An error occurred while checking P1.1.3: ${error.message}`]
      }
    } finally {
      await session.close()
    }
  }
}
