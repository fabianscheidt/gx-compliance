import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.1.1">P1.1.1 labelling criterion</a>.
 */
export class ServiceOfferingHasLegallyBindingActFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasLegallyBindingActFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(`Checking service offering has legally binding act for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = []
    for (const result of results) {
      if (
        result.legalDocuments.length < 1 ||
        !result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#LegallyBindingAct')
      ) {
        this.logger.error(`P1.1.1 - No legally binding act is provided for the service offering ${result.serviceOfferingId} for VPUUID ${vpUUID}...`)
        errorMessages.push(`P1.1.1 - No legally binding act is provided for the service offering ${result.serviceOfferingId}`)
      }
    }

    if (errorMessages.length) {
      return {
        conforms: false,
        results: errorMessages
      }
    }

    return new ValidationResult()
  }
}
