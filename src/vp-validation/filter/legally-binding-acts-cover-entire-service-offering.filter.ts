import { Logger } from '@nestjs/common'

import { Driver, Session } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { graphValueFormat } from '../../common/utils/graph-value-format'
import { ValidationFilter } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.1.4">P1.1.4 labelling criterion</a>.
 */
export class LegallyBindingActsCoverEntireServiceOfferingFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(LegallyBindingActsCoverEntireServiceOfferingFilter.name)

  async doFilter(vpUUID: string, _verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Checking that legally binding acts cover the entire service offering for VPUUID ${vpUUID}...`)

    const query = `MATCH (serviceOffering)-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_development_ServiceOffering_)
      WHERE serviceOffering.vpID="${vpUUID}" 
      CALL {
        WITH serviceOffering
        OPTIONAL MATCH (serviceScope)<-[:_https_w3id_org_gaia_x_development_serviceScope_]-()
          <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-(serviceOffering)
        RETURN COUNT(serviceScope) AS serviceScopeCount
      }
      CALL {
        WITH serviceOffering
        OPTIONAL MATCH (aggregationOfResources)<-[:_https_w3id_org_gaia_x_development_aggregationOfResources_]-()
          <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-(serviceOffering)
        RETURN COUNT(aggregationOfResources) AS aggregationOfResourcesCount
      }
      CALL {
        WITH serviceOffering
        OPTIONAL MATCH (dependsOn)<-[:_https_w3id_org_gaia_x_development_dependsOn_]-()
          <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-(serviceOffering)
        RETURN COUNT(dependsOn) AS dependsOnCount
      }
      RETURN serviceOffering.value AS serviceOfferingId, serviceScopeCount, aggregationOfResourcesCount, dependsOnCount`
    const session: Session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      this.logger.debug(`Found ${results.records.length} service offerings for VPUUID ${vpUUID}...`)

      const invalidServiceOfferings: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')

        if (
          !record.get('serviceScopeCount').toNumber() &&
          (!record.get('aggregationOfResourcesCount').toNumber() || !record.get('dependsOnCount').toNumber())
        ) {
          this.logger.error(
            `${serviceOfferingId} is missing a service scope or, an aggregation of resources and a depends on attribute for VPUUID ${vpUUID}...`
          )
          invalidServiceOfferings.push(serviceOfferingId)
        }
      }

      if (invalidServiceOfferings.length) {
        return {
          conforms: false,
          results: invalidServiceOfferings.map(
            serviceOfferingId =>
              `P1.1.4 - Service offering ${graphValueFormat(
                serviceOfferingId
              )} is missing a service scope or, an aggregation of resources and a depends on attribute`
          )
        }
      }
    } catch (error) {
      this.logger.error(
        `An error occurred while checking that legally binding acts cover the entire service offering for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`An error occurred while checking that legally binding acts cover the entire service offering: ${error.message}`]
      }
    } finally {
      await session.close()
    }

    return new ValidationResult()
  }
}
