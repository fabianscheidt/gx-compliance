import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.2">P3.1.2 labelling criterion</a>.
 */
export class ServiceOfferingHasInformationSecurityPoliciesFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasInformationSecurityPoliciesFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(`Checking that service offerings have information security policies for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(
        result => !result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#InformationSecurityPolicies')
      )
      .map(result => {
        this.logger.error(
          `P3.1.2 - Service offering ${result.serviceOfferingId} is missing information security policies in its legal documents for VPUUID ${vpUUID}...`
        )
        return `P3.1.2 - Service offering ${result.serviceOfferingId} is missing information security policies in its legal documents`
      })

    if (errorMessages.length > 0) {
      return {
        conforms: false,
        results: errorMessages
      }
    }

    return new ValidationResult()
  }
}
