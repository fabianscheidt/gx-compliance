import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithServiceOfferingWithLegallyBindingActWithoutEEA from './fixtures/service-offering-legally-binding-acts-have-governing-law-country/vp-with-service-offering-with-legally-binding-act-without-EEA.json'
import vpWithServiceOfferingWithLegallyBindingActWithoutGoverningLaws from './fixtures/service-offering-legally-binding-acts-have-governing-law-country/vp-with-service-offering-with-legally-binding-act-without-governing-laws.json'
import vpWithValidServiceOffering from './fixtures/service-offering-legally-binding-acts-have-governing-law-country/vp-with-valid-service-offering.json'
import { ServiceOfferingLegallyBindingActsHaveGoverningLawCountry } from './service-offering-legally-binding-acts-have-governing-law.country'

describe('ServiceOfferingLegallyBindingActsHaveGoverningLawCountry', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingLegallyBindingActsHaveGoverningLawCountry()

    it('should be conform with at least a document having governingLawCountries containing an EEA country', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)
      const validationResult: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(validationResult.conforms).toBe(true)
      expect(validationResult.results).toHaveLength(0)
    })

    it('should not be conform if no document contains a governing law country', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithServiceOfferingWithLegallyBindingActWithoutGoverningLaws)
      const validationResult: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(validationResult.conforms).toBe(false)
      expect(validationResult.results).toEqual([
        `P1.1.5 - Service offering <https://example.org/service-offering.json> does not have a governing law country for legally binding act https://example.org/service-offering/legally-binding-act-3.pdf`,
        `P1.1.5 - Service offering <https://example.org/service-offering.json> does not have a governing law country for legally binding act https://example.org/service-offering/legally-binding-act-2.pdf`
      ])
    })

    it('should not be conform if no document contains a governing law country in EEA', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithServiceOfferingWithLegallyBindingActWithoutEEA)
      const validationResult: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(validationResult.conforms).toBe(false)
      expect(validationResult.results).toEqual([
        `P1.1.2 - Service offering <https://example.org/service-offering.json> with legally binding act https://example.org/service-offering/legally-binding-act-2.pdf must have at least one EEA governing law country`,
        `P1.1.2 - Service offering <https://example.org/service-offering.json> with legally binding act https://example.org/service-offering/legally-binding-act.pdf must have at least one EEA governing law country`
      ])
    })
  })
})
