import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import vpWithServiceOfferingWithInvalidBindingActUrl from '../filter/fixtures/service-offering-has-legally-binding-act-filter/vp-with-service-offering-with-invalid-legally-binding-act-url.json'
import vpWithServiceOfferingWithoutLegallyBindingAct from '../filter/fixtures/service-offering-has-legally-binding-act-filter/vp-with-service-offering-without-legally-binding-act.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-legally-binding-act-filter/vp-with-valid-service-offering.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasLegallyBindingActFilter } from './service-offering-has-legally-binding-act.filter'

describe('ServiceOfferingHasLegallyBindingActFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasLegallyBindingActFilter()

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a negative conformity with a reason when legally binding acts are missing', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithServiceOfferingWithoutLegallyBindingAct)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        `P1.1.1 - No legally binding act is provided for the service offering <https://example.org/service-offering.json>`
      ])
    })

    it('should return a negative conformity with a reason when at least one legally binding act is invalid', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithServiceOfferingWithInvalidBindingActUrl)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        `Legal document with URL <this is invalid for sure> is invalid: Error: Legal document url 'this is invalid for sure' is invalid`
      ])
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
    })
  })
})
