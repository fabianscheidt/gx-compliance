import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import vpWithASingleNonTrustedRegistrationNumbersIssuer from '../filter/fixtures/registration-numbers-are-from-trusted-issuers/vp-with-a-single-non-trusted-registration-numbers-issuer.json'
import vpWithRegistrationNumbersFromTrustedIssuers from '../filter/fixtures/registration-numbers-are-from-trusted-issuers/vp-with-registration-numbers-from-trusted-issuers.json'
import { TrustedNotaryIssuerService } from '../service/trusted-notary-issuer.service'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { RegistrationNumbersAreFromTrustedIssuersFilter } from './registration-numbers-are-from-trusted-issuers.filter'

describe('RegistrationNumbersAreFromTrustedIssuersFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const trustedNotaryIssuerServiceMock: TrustedNotaryIssuerService = {
      isTrusted: (issuer: string) => issuer.includes('trusted')
    } as unknown as TrustedNotaryIssuerService

    let filter: ValidationFilter

    beforeEach(() => {
      jest.resetAllMocks()

      filter = new RegistrationNumbersAreFromTrustedIssuersFilter(trustedNotaryIssuerServiceMock)
    })

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithRegistrationNumbersFromTrustedIssuers)
      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a negative conformity with reason when at least one registration number issuer is not trusted', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithASingleNonTrustedRegistrationNumbersIssuer)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        'malicious.com is not a trusted registration number issuer',
        'not-trustworthy.com is not a trusted registration number issuer'
      ])
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An error occurred while checking registration number trusted issuers: Test error'])
    })
  })
})
