import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.17>P3.1.17 labelling criterion</a>.
 */
export class ServiceOfferingHasComplianceAssuranceFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasComplianceAssuranceFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(`Checking that service offerings have compliance assurance for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(result => !result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#ComplianceAssurance'))
      .map(result => {
        this.logger.error(
          `P3.1.17 - Service offering ${result.serviceOfferingId} is missing compliance assurance in its legal documents for VPUUID ${vpUUID}...`
        )
        return `P3.1.17 - Service offering ${result.serviceOfferingId} is missing compliance assurance in its legal documents`
      })

    return {
      conforms: !errorMessages.length,
      results: errorMessages
    }
  }
}
