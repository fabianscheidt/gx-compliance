import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import vpWithMissingDataProtectionRegulationDocument from '../filter/fixtures/service-offering-has-data-protection-and-service-agreement/vp-missing-data-protection-and-service-agreement.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-data-protection-and-service-agreement/vp-valid-data-protection-and-service-agreement.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHaveDataProtectionAndServiceAgreementFilter } from './service-offering-have-data-protection-and-service-agreement.filter'

describe('ServiceOfferingHaveDataProtectionAndServiceAgreementFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHaveDataProtectionAndServiceAgreementFilter()

    it('should pass with a valid presentation', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should not pass when Data Protection Regulation Measures And Service Agreement Offer documents are missing', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithMissingDataProtectionRegulationDocument)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toHaveLength(4)
      expect(result.results).toContain(
        'P2.1.1 - Service offering <https://gaia-x.eu/service-offering1.json> is missing a DataProtectionRegulationMeasures legal document'
      )
      expect(result.results).toContain(
        'P2.1.1 - Service offering <https://gaia-x.eu/service-offering2.json> is missing a ServiceAgreementOffer legal document'
      )
      expect(result.results).toContain(
        'P2.1.1 - Service offering <https://gaia-x.eu/service-offering3.json> is missing a ServiceAgreementOffer legal document'
      )
      expect(result.results).toContain(
        'P2.1.1 - Service offering <https://gaia-x.eu/service-offering3.json> is missing a DataProtectionRegulationMeasures legal document'
      )
    })

    it('should not pass when query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
    })
  })
})
