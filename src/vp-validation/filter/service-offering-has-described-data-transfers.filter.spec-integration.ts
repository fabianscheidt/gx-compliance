import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithInvalidServiceOfferings from './fixtures/service-offering-has-described-data-transfers-filter/vp-with-invalid-service-offerings.json'
import vpWithValidServiceOffering from './fixtures/service-offering-has-described-data-transfers-filter/vp-with-valid-service-offerings.json'
import { ServiceOfferingHasDescribedDataTransfersFilter } from './service-offering-has-described-data-transfers.filter'

describe('ServiceOfferingHasDescribedDataTransfersFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasDescribedDataTransfersFilter()

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a negative conformity with a reason when data transfers might be done to unsecure countries', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithInvalidServiceOfferings)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        `P2.2.4 - Service offering <https://example.org/service-offering-with-unsecure-data-transfers.json> might transfer data to an unsecure third country (according to GDPR's Chapter V) with code CN`,
        `P2.2.4 - Service offering <https://example.org/service-offering-with-unsecure-data-transfers.json> might transfer data to an unsecure third country (according to GDPR's Chapter V) with code BU`
      ])
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        'An error occurred while checking that the service offering has described possible personal data transfers: Test error'
      ])
    })
  })
})
