import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import mismatchingInvolvedPartiesVP from './fixtures/legal-document-has-involved-parties/vp-mismatching-involved-parties.json'
import missingInvolvedPartiesVP from './fixtures/legal-document-has-involved-parties/vp-missing-involved-parties.json'
import validVP from './fixtures/legal-document-has-involved-parties/vp-valid-involved-parties.json'
import { LegalDocumentHasInvolvedParties } from './legal-document-has-involved-parties.filter'

describe('LegalDocumentHasInvolvedParties', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new LegalDocumentHasInvolvedParties()

    it('should pass with valid involved parties and corresponding legalPerson', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, validVP)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should not pass with missing involved parties values', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, missingInvolvedPartiesVP)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        `P1.1.3 - Legally binding act of service offering <https://gaia-x.eu/service-offering.json> is missing involved parties`
      ])
    })

    it('should not pass with valid involved parties and missing corresponding legalPerson', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, mismatchingInvolvedPartiesVP)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        `P1.1.3 - Legally binding act of service offering <https://gaia-x.eu/service-offering.json> is referencing an involved party <https://example.org/participant-2.json#cs> that does not match any legal person`,
        `P1.1.3 - Legally binding act of service offering <https://gaia-x.eu/service-offering.json> is referencing an involved party <https://example.org/participant.json#cs> that does not match any legal person`
      ])
    })

    it('should not pass when query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An error occurred while checking P1.1.3: Test error'])
    })
  })
})
