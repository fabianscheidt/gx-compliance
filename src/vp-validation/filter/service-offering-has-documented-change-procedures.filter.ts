import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.2.3">P1.2.3 labelling criterion</a>.
 */
export class ServiceOfferingHasDocumentedChangeProceduresFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasDocumentedChangeProceduresFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(`Checking that service offerings have documented change procedures for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(result => !result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#ChangeProceduresDocument'))
      .map(result => {
        this.logger.error(
          `P1.2.3 - Service offering ${result.serviceOfferingId} is missing a change procedures documentation in its legal documents for VPUUID ${vpUUID}...`
        )
        return `P1.2.3 - Service offering ${result.serviceOfferingId} is missing a change procedures documentation in its legal documents`
      })

    if (errorMessages.length > 0) {
      return {
        conforms: false,
        results: errorMessages
      }
    }

    return new ValidationResult()
  }
}
