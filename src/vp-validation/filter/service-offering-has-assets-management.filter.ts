import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.5">P3.1.5 labelling criterion</a>.
 */
export class ServiceOfferingHasAssetsManagementFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasAssetsManagementFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(`Checking that service offerings have assets management for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(result => !result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#AssetsManagement'))
      .map(result => {
        this.logger.error(
          `P3.1.5 - Service offering ${result.serviceOfferingId} is missing assets management in its legal documents for VPUUID ${vpUUID}...`
        )
        return `P3.1.5 - Service offering ${result.serviceOfferingId} is missing assets management in its legal documents`
      })

    if (errorMessages.length > 0) {
      return {
        conforms: false,
        results: errorMessages
      }
    }

    return new ValidationResult()
  }
}
