import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import vpWithMissingRoleAndResponsibilityDocument from '../filter/fixtures/service-offering-has-roles-and-responsibilities/vp-missing-roles-and-responsibilities.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-roles-and-responsibilities/vp-valid-roles-and-responsabilities.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasRoleAndResponsibilitiesFilter } from './service-offering-has-role-and-responsibilities.filter'

describe('ServiceOfferingHasRolesAndResponsibilitiesFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasRoleAndResponsibilitiesFilter()

    it('should pass with a valid presentation', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should not pass when RoleAndResponsibility document is missing', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithMissingRoleAndResponsibilityDocument)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)

      expect(result.results).toEqual([
        'P2.1.2 - Service offering <https://gaia-x.eu/service-offering1.json> is missing a Role and Responsibilities legal document',
        'P2.1.2 - Service offering <https://gaia-x.eu/service-offering.json> is missing a Role and Responsibilities legal document'
      ])
    })

    it('should not pass when query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
    })
  })
})
