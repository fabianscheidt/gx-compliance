import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.2.2">P1.2.2 labelling criterion</a>.
 */
export class ServiceOfferingHasDataUsageRightsForPartiesFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasDataUsageRightsForPartiesFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(`Checking that service offerings have data usage rights of parties for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = []
    for (const result of results) {
      if (!result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#LegallyBindingAct')) {
        this.logger.error(
          `P1.2.2 - Service offering ${result.serviceOfferingId} is missing a legally binding act in its legal documents for VPUUID ${vpUUID}...`
        )
        errorMessages.push(`P1.2.2 - Service offering ${result.serviceOfferingId} is missing a legally binding act in its legal documents`)
      }

      if (!result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#CustomerDataProcessingTerms')) {
        this.logger.error(
          `P1.2.2 - Service offering ${result.serviceOfferingId} is missing customer data processing terms in its legal documents for VPUUID ${vpUUID}...`
        )
        errorMessages.push(`P1.2.2 - Service offering ${result.serviceOfferingId} is missing customer data processing terms in its legal documents`)
      }

      if (!result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#CustomerDataAccessTerms')) {
        this.logger.error(
          `P1.2.2 - Service offering ${result.serviceOfferingId} is missing customer data access terms in its legal documents for VPUUID ${vpUUID}...`
        )
        errorMessages.push(`P1.2.2 - Service offering ${result.serviceOfferingId} is missing customer data access terms in its legal documents`)
      }
    }

    if (errorMessages.length > 0) {
      return {
        conforms: false,
        results: errorMessages
      }
    }

    return new ValidationResult()
  }
}
