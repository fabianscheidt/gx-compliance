import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { ValidationFilter } from './common/validation-filter.interface'

export class ServiceOfferingIssuerMatchesProviderIssuerFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingIssuerMatchesProviderIssuerFilter.name)

  async doFilter(vpUUID: string, _verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Checking that service offering issuer and provider issuer match for VPUUID ${vpUUID}...`)

    const query = `MATCH (serviceOfferingIssuer)<-[:_https_www_w3_org_2018_credentials_issuer_]-(serviceOffering)
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_development_ServiceOffering_)
      WHERE serviceOfferingIssuer.vpID="${vpUUID}"
      CALL {
        WITH serviceOffering
        OPTIONAL MATCH (serviceOffering)-[:_https_www_w3_org_2018_credentials_credentialSubject_]->()
          -[:_https_w3id_org_gaia_x_development_providedBy_]->()
          <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-()
          -[:_https_www_w3_org_2018_credentials_issuer_]->(participantIssuer)
        RETURN DISTINCT participantIssuer
      }
      RETURN serviceOfferingIssuer,participantIssuer`

    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      this.logger.debug(`Found ${results.records.length} service offerings with providers for VPUUID ${vpUUID}...`)
      const issuersMatch: boolean = results.records
        .map(record => {
          return record.get('serviceOfferingIssuer')?.properties?.value === record.get('participantIssuer')?.properties?.value
        })
        .reduce((a, b) => a && b, true)

      return {
        conforms: issuersMatch,
        results: issuersMatch ? [] : ['Service offering issuer and provider issuer do not match']
      }
    } catch (error) {
      this.logger.error(
        `An error occurred while checking that service offering issuer and provider issuer match for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`An error occurred while checking that service offering issuer and provider issuer match: ${error.message}`]
      }
    } finally {
      await session.close()
    }
  }
}
