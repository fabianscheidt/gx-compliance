import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import vpWithDataResourceIssuersMatchingProducerIssuers from '../filter/fixtures/data-resource-issuer-matches-producer-issuer-filter/vp-with-data-resource-issuers-matching-producer-issuers.json'
import vpWithDataResourceIssuersNotMatchingProducerIssuers from '../filter/fixtures/data-resource-issuer-matches-producer-issuer-filter/vp-with-data-resource-issuers-not-matching-producer-issuers.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { DataResourceIssuerMatchesProducerIssuerFilter } from './data-resource-issuer-matches-producer-issuer.filter'

describe('DataResourceIssuerMatchesProducerIssuerFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new DataResourceIssuerMatchesProducerIssuerFilter()

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithDataResourceIssuersMatchingProducerIssuers)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a negative conformity with a reason when data resource and producer issuer do not match', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithDataResourceIssuersNotMatchingProducerIssuers)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['Data resource issuer and producer issuer do not match'])
    })

    it('should return a positive conformity when no data resource is present', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, {})

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An error occurred while checking that data resource issuer and producer issuer match: Test error'])
    })
  })
})
