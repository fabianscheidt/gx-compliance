import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import vpWithInvalidServiceOfferings from '../filter/fixtures/service-offering-has-data-usage-rights-for-parties-filter/vp-with-invalid-service-offerings.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-data-usage-rights-for-parties-filter/vp-with-valid-service-offering.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasDataUsageRightsForPartiesFilter } from './service-offering-has-data-usage-rights-for-parties.filter'

describe('ServiceOfferingHasDataUsageRightsForPartiesFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasDataUsageRightsForPartiesFilter()

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a negative conformity with a reason when legal documents are missing from service offerings', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithInvalidServiceOfferings)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)

      expect(result.results).toEqual([
        'P1.2.2 - Service offering <https://example.org/service-offering-without-legally-binding-act.json> is missing a legally binding act in its legal documents',
        'P1.2.2 - Service offering <https://example.org/service-offering-without-customer-data-processing-terms.json> is missing customer data processing terms in its legal documents',
        'P1.2.2 - Service offering <https://example.org/service-offering-without-customer-data-access-terms.json> is missing customer data access terms in its legal documents'
      ])
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
    })
  })
})
