import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.1.2">P2.1.2 labelling criterion</a>.
 */
export class ServiceOfferingHasRoleAndResponsibilitiesFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasRoleAndResponsibilitiesFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(`Checking that service offerings have at least one Role and Responsibilities legal document for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(result => !result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#RoleAndResponsibilities'))
      .map(result => {
        this.logger.error(
          `P2.1.2 - Service offering ${result.serviceOfferingId} is missing a Role and Responsibilities legal document for VPUUID ${vpUUID}...`
        )
        return `P2.1.2 - Service offering ${result.serviceOfferingId} is missing a Role and Responsibilities legal document`
      })

    if (errorMessages.length > 0) {
      return {
        conforms: false,
        results: errorMessages
      }
    }

    return new ValidationResult()
  }
}
