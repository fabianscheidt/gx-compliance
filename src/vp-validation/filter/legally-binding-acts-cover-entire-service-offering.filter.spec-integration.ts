import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithInvalidServiceOfferings from './fixtures/legally-binding-acts-cover-entire-service-offering-filter/vp-with-invalid-service-offerings.json'
import vpWithValidServiceOfferings from './fixtures/legally-binding-acts-cover-entire-service-offering-filter/vp-with-valid-service-offerings.json'
import { LegallyBindingActsCoverEntireServiceOfferingFilter } from './legally-binding-acts-cover-entire-service-offering.filter'

describe('LegallyBindingActsCoverEntireServiceOfferingFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new LegallyBindingActsCoverEntireServiceOfferingFilter()

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOfferings)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a negative conformity with a reason when some service offerings are missing required attributes', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithInvalidServiceOfferings)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        'P1.1.4 - Service offering example.org/service-offering-without-aggregation-of-resources.json is missing a service scope or, an aggregation of resources and a depends on attribute',
        'P1.1.4 - Service offering example.org/service-offering-without-any-required-attribute.json is missing a service scope or, an aggregation of resources and a depends on attribute',
        'P1.1.4 - Service offering example.org/service-offering-without-depends-on.json is missing a service scope or, an aggregation of resources and a depends on attribute'
      ])
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An error occurred while checking that legally binding acts cover the entire service offering: Test error'])
    })
  })
})
