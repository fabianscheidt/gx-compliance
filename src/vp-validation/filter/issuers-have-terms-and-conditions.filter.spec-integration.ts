import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import vpWithIssuersAndOneMissingTermsAndConditions from '../filter/fixtures/issuers-have-terms-and-conditions-filter/vp-with-issuers-and-one-missing-terms-and-conditions.json'
import vpWithIssuersWithInvalidTermsAndConditions from '../filter/fixtures/issuers-have-terms-and-conditions-filter/vp-with-issuers-with-invalid-terms-and-conditions.json'
import vpWithIssuersWithTermsAndConditions from '../filter/fixtures/issuers-have-terms-and-conditions-filter/vp-with-issuers-with-terms-and-conditions.json'
import vpWithUntrustedIssuers from '../filter/fixtures/issuers-have-terms-and-conditions-filter/vp-with-untrusted-issuers.json'
import { TrustedNotaryIssuerService } from '../service/trusted-notary-issuer.service'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { IssuersHaveTermsAndConditionsFilter } from './issuers-have-terms-and-conditions.filter'

describe('IssuersHaveTermsAndConditionsFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const trustedNotaryIssuerServiceMock: TrustedNotaryIssuerService = {
      isTrusted: (issuer: string) => issuer.includes('trusted')
    } as unknown as TrustedNotaryIssuerService

    let filter: ValidationFilter

    beforeEach(() => {
      jest.resetAllMocks()

      filter = new IssuersHaveTermsAndConditionsFilter(trustedNotaryIssuerServiceMock)
    })

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithIssuersWithTermsAndConditions)
      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a negative conformity with reason when an issuer does not have terms and conditions', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithIssuersAndOneMissingTermsAndConditions)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['second-trusted-issuer.com is missing a gx:Issuer entity with terms and conditions'])
    })

    it('should return a negative conformity with reason when an issuer has invalid terms and conditions', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithIssuersWithInvalidTermsAndConditions)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        'first-trusted-issuer.com has an incorrect Gaia-X terms and conditions hash',
        'unused-trusted-issuer.com has an incorrect Gaia-X terms and conditions hash'
      ])
    })

    it('should return a negative conformity with reason when an issuer is not trusted', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithUntrustedIssuers)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['second-malicious-issuer.com is not a trusted issuer'])
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An error occurred while checking registration number trusted issuers: Test error'])
    })
  })
})
