import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.7">P3.1.7 labelling criterion</a>.
 * <a href="https://gitlab.com/gaia-x/technical-committee/service-characteristics-working-group/service-characteristics/-/blame/develop/single-point-of-truth/legal-document.yaml?ref_type=heads#L81">OperationalSecurity Shacl definition</a>.
 */
export class ServiceOfferingHasOperationalSecurityFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasOperationalSecurityFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(`Checking that service offerings have at least one Operational Security legal document for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(result => !result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#OperationalSecurity'))
      .map(result => {
        this.logger.error(
          `P3.1.7 - Service offering ${result.serviceOfferingId} is missing an Operational Security legal document for VPUUID ${vpUUID}...`
        )
        return `P3.1.7 - Service offering ${result.serviceOfferingId} is missing an Operational Security legal document`
      })

    return {
      conforms: !errorMessages.length,
      results: errorMessages
    }
  }
}
