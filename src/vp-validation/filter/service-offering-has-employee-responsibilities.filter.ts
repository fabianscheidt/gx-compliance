import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.4">P3.1.4 labelling criterion</a>.
 */
export class ServiceOfferingHasEmployeeResponsibilitiesFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasEmployeeResponsibilitiesFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(`Checking that service offerings have customer auditing rights for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(result => !result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#EmployeeResponsibilities'))
      .map(result => {
        this.logger.error(
          `P3.1.4 - Service offering ${result.serviceOfferingId} is missing employee responsibilities in its legal documents for VPUUID ${vpUUID}...`
        )
        return `P3.1.4 - Service offering ${result.serviceOfferingId} is missing employee responsibilities in its legal documents`
      })

    if (errorMessages.length > 0) {
      return {
        conforms: false,
        results: errorMessages
      }
    }

    return new ValidationResult()
  }
}
