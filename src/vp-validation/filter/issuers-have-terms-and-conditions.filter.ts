import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'
import { ValidationResult } from 'src/common/dto'

import { VerifiablePresentation } from '../../common/constants'
import { graphValueFormat } from '../../common/utils/graph-value-format'
import { TrustedNotaryIssuerService } from '../service/trusted-notary-issuer.service'
import { ValidationFilter } from './common/validation-filter.interface'

export class IssuersHaveTermsAndConditionsFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(IssuersHaveTermsAndConditionsFilter.name)

  constructor(private readonly trustedNotaryIssuerService: TrustedNotaryIssuerService) {}

  async doFilter(vpUUID: string, _verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Searching for registration number issuers VPUUID ${vpUUID}...`)

    const session = driver.session()
    const query = `MATCH ()-[:_https_www_w3_org_2018_credentials_issuer_]->(nodeIssuer)
      WITH DISTINCT nodeIssuer
      WHERE nodeIssuer.vpID="${vpUUID}"
      OPTIONAL MATCH (nodeIssuer)<-[:_https_www_w3_org_2018_credentials_issuer_]-(issuer)
        -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_development_Issuer_)
      CALL {
        WITH issuer
        OPTIONAL MATCH (issuer)-[:_https_www_w3_org_2018_credentials_credentialSubject_]->()
          -[:_https_w3id_org_gaia_x_development_gaiaxTermsAndConditions_]->(termsAndConditions)
        RETURN termsAndConditions
      }
      RETURN nodeIssuer.value AS issuer, termsAndConditions.value AS termsAndConditionsHash;`

    try {
      const results = await session.executeRead(tx => tx.run(query))
      this.logger.debug(`Found ${results.records.length} issuers for VPUUID ${vpUUID}}`)

      const missingTermsAndConditions: string[] = []
      const invalidTermsAndConditions: string[] = []
      const untrustedIssuers: string[] = []
      for (const result of results.records) {
        const issuerValue: string = graphValueFormat(result.get('issuer'))

        if (!result.get('termsAndConditionsHash')) {
          this.logger.error(`${issuerValue} is missing a gx:Issuer entity with terms and conditions for VPUUID ${vpUUID}`)
          missingTermsAndConditions.push(issuerValue)
        }

        if (
          result.get('termsAndConditionsHash') &&
          result.get('termsAndConditionsHash') != '4bd7554097444c960292b4726c2efa1373485e8a5565d94d41195214c5e0ceb3'
        ) {
          this.logger.error(`${issuerValue} has an incorrect Gaia-X terms and conditions hash for VPUUID ${vpUUID}`)
          invalidTermsAndConditions.push(issuerValue)
        }

        if (!(await this.trustedNotaryIssuerService.isTrusted(issuerValue))) {
          this.logger.error(`${issuerValue} is not a trusted issuer for VPUUID ${vpUUID}`)
          untrustedIssuers.push(issuerValue)
        }
      }

      if (missingTermsAndConditions.length > 0 || invalidTermsAndConditions.length > 0 || untrustedIssuers.length > 0) {
        return {
          conforms: false,
          results: missingTermsAndConditions
            .map(issuer => `${issuer} is missing a gx:Issuer entity with terms and conditions`)
            .concat(invalidTermsAndConditions.map(issuer => `${issuer} has an incorrect Gaia-X terms and conditions hash`))
            .concat(untrustedIssuers.map(issuer => `${issuer} is not a trusted issuer`))
        }
      }

      return {
        conforms: true,
        results: []
      }
    } catch (error) {
      this.logger.error(`An error occurred while checking registration number trusted issuers for VPUUID ${vpUUID}. Error: ${error.message}`)

      return {
        conforms: false,
        results: [`An error occurred while checking registration number trusted issuers: ${error.message}`]
      }
    } finally {
      await session.close()
    }
  }
}
