import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithServiceOfferingWithCustomerInstructions from './fixtures/service-offering-has-customer-instructions-filter/vp-with-service-offering-with-customer-instructions.json'
import vpWithServiceOfferingsWithoutCustomerInstructions from './fixtures/service-offering-has-customer-instructions-filter/vp-with-service-offerings-without-customer-instructions.json'
import { ServiceOfferingHasCustomerInstructionsFilter } from './service-offering-has-customer-instructions.filter'

describe('ServiceOfferingHasCustomerInstructionsFilter', () => {
  memgraphTestSuite(driver => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasCustomerInstructionsFilter()

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithServiceOfferingWithCustomerInstructions)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a non conform result with reason when customer instructions are missing', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithServiceOfferingsWithoutCustomerInstructions)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        `P2.2.2 - Service offering <https://example.org/service-offering-without-customer-instructions-means.json> is missing customer instruction means`,
        `P2.2.1 - Service offering <https://example.org/service-offering-without-customer-instructions-terms.json> is missing customer instruction terms`,
        `P2.2.1 - Service offering <https://example.org/service-offering-without-customer-instructions.json> is missing customer instruction terms`,
        `P2.2.2 - Service offering <https://example.org/service-offering-without-customer-instructions.json> is missing customer instruction means`
      ])
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An error occurred while checking that service offerings have customer instructions : Test error'])
    })
  })
})
