import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import vpWithMissingAccessControlManagementDocument from '../filter/fixtures/service-offering-has-access-control-management/vp-missing-access-control-management.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-access-control-management/vp-valid-access-control-management.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasAccessControlManagementFilter } from './service-offering-has-access-control-management.filter'

describe('ServiceOfferingHasAccessControlManagementFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasAccessControlManagementFilter()

    it('should pass with a valid presentation', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should not pass when AccessControlManagement document is missing', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithMissingAccessControlManagementDocument)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)

      expect(result.results).toEqual([
        'P3.1.8 - Service offering <https://gaia-x.eu/service-offering1.json> is missing an Access Control Management legal document',
        'P3.1.8 - Service offering <https://gaia-x.eu/service-offering.json> is missing an Access Control Management legal document'
      ])
    })

    it('should not pass when query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
    })
  })
})
