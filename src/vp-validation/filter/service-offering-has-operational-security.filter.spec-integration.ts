import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import vpWithMissingOperationalSecurityDocument from '../filter/fixtures/service-offering-has-operational-security/vp-missing-operational-security.json'
import vpWithValidServiceOffering from '../filter/fixtures/service-offering-has-operational-security/vp-valid-operational-security.json'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import { ServiceOfferingHasOperationalSecurityFilter } from './service-offering-has-operational-security.filter'

describe('ServiceOfferingHasOperationalSecurityFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasOperationalSecurityFilter()

    it('should pass with a valid presentation', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should not pass when OperationalSecurity document is missing', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithMissingOperationalSecurityDocument)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)

      expect(result.results).toEqual([
        'P3.1.7 - Service offering <https://gaia-x.eu/service-offering1.json> is missing an Operational Security legal document',
        'P3.1.7 - Service offering <https://gaia-x.eu/service-offering.json> is missing an Operational Security legal document'
      ])
    })

    it('should not pass when query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An unexpected error has occurred while collecting legal documents: Test error'])
    })
  })
})
