import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { ValidationFilter } from './common/validation-filter.interface'

interface CustomerInstructionsProperties {
  type: string
  value: string
}

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.2.1">P2.2.1</a> &
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P2.2.2">P2.2.2</a>
 * labelling criterion</a>.
 */
export class ServiceOfferingHasCustomerInstructionsFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingHasCustomerInstructionsFilter.name)

  async doFilter(vpUUID: string, _verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Checking that service offerings have customer instructions for VPUUID ${vpUUID}...`)

    const query = `MATCH (:_https_w3id_org_gaia_x_development_ServiceOffering_)<-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]-(serviceOffering)
        -[:_https_www_w3_org_2018_credentials_credentialSubject_]->(credentialSubject)
      WHERE serviceOffering.vpID="${vpUUID}"
      CALL {
        WITH credentialSubject
        OPTIONAL MATCH (credentialSubject)-[:_https_w3id_org_gaia_x_development_customerInstructions_]->()
          -[r*]->(property)
        RETURN COLLECT({ type: type(r[0]), value: property.value }) AS properties
      }
      RETURN serviceOffering.value as serviceOfferingId, properties`

    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')
        const properties: CustomerInstructionsProperties[] = record.get('properties')

        if (!properties.some(property => property.type === '_https_w3id_org_gaia_x_development_terms_')) {
          this.logger.error(`P2.2.1 - Service offering ${serviceOfferingId} is missing customer instruction terms for VPUUID ${vpUUID}...`)
          errorMessages.push(`P2.2.1 - Service offering ${serviceOfferingId} is missing customer instruction terms`)
        }

        if (!properties.some(property => property.type === '_https_w3id_org_gaia_x_development_means_')) {
          this.logger.error(`P2.2.2 - Service offering ${serviceOfferingId} is missing customer instruction means for VPUUID ${vpUUID}...`)
          errorMessages.push(`P2.2.2 - Service offering ${serviceOfferingId} is missing customer instruction means`)
        }
      }

      return {
        conforms: errorMessages.length < 1,
        results: errorMessages
      }
    } catch (error) {
      this.logger.error(
        `An error occurred while checking that service offerings have customer instructions for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`An error occurred while checking that service offerings have customer instructions : ${error.message}`]
      }
    } finally {
      await session.close()
    }
  }
}
