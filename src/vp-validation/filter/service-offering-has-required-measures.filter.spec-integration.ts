import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import missingRequiredMeasures from './fixtures/service-offering-has-required-measures/vp-missing-required-measures.json'
import validVP from './fixtures/service-offering-has-required-measures/vp-valid-required-measures.json'
import { ServiceOfferingHasRequiredMeasuresFilter } from './service-offering-has-required-measures.filter'

describe('ServiceOfferingHasRequiredMeasuresFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasRequiredMeasuresFilter()

    it('should pass with valid required measures', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, validVP)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result).toBeDefined()
      expect(result.conforms).toBeTruthy()
      expect(result.results).toHaveLength(0)
    })

    it('should not pass with missing required measures', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, missingRequiredMeasures)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result).toBeDefined()
      expect(result.conforms).toBeFalsy()
      expect(result.results).toHaveLength(2)
      expect(result.results).toContain('P2.1.3 - Service Offering <https://gaia-x.eu/service-offering1.json> must define requiredMeasures.')
      expect(result.results).toContain('P2.1.3 - Service Offering <https://gaia-x.eu/service-offering2.json> must define requiredMeasures.')
    })

    it('should not pass when query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An error occurred while checking P2.1.3: Test error'])
    })
  })
})
