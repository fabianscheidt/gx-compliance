import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithValidServiceOffering from './fixtures/service-offering-has-a-resource-with-address-filter/vp-with-resource-with-address.json'
import vpWithoutAddressInTheResource from './fixtures/service-offering-has-a-resource-with-address-filter/vp-with-resource-without-address.json'
import vpWithoutResourceInTheServiceOffering from './fixtures/service-offering-has-a-resource-with-address-filter/vp-without-resource.json'
import { ServiceOfferingHasAResourceWithAddressFilter } from './service-offering-has-a-resource-with-address.filter'

describe('ServiceOfferingHasAResourceWithAddressFilter', () => {
  memgraphTestSuite(driver => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasAResourceWithAddressFilter()
    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidServiceOffering)
      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)
      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })
    it('should return a non conform result if there are no resources', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithoutResourceInTheServiceOffering)
      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)
      expect(result.conforms).toBe(false)
      expect(result.results).toHaveLength(1)
      expect(result.results).toContain('P1.2.5 - No resource with an address provided in the VerifiablePresentation')
    })
    it('should return a non conform result if there are no address in the resources', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithoutAddressInTheResource)
      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)
      expect(result.conforms).toBe(false)
      expect(result.results).toHaveLength(1)
      expect(result.results).toContain('P1.2.5 - No resource with an address provided in the VerifiablePresentation')
    })
    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An error occurred while checking that service offerings have a resource with an address : Test error'])
    })
  })
})
