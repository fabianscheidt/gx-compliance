import { Logger } from '@nestjs/common'

import { ValidationResult } from '../../common/dto'
import { LegalDocumentFilter, ServiceOfferingLegalDocuments } from './common/legal-document.filter'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.16">P3.1.16 labelling criterion</a>.
 */
export class ServiceOfferingHasBusinessContinuityMeasuresFilter extends LegalDocumentFilter {
  constructor() {
    super(new Logger(ServiceOfferingHasBusinessContinuityMeasuresFilter.name))
  }

  verifyLegalDocuments(vpUUID: string, results: ServiceOfferingLegalDocuments[]): ValidationResult {
    this.logger.debug(`Checking that service offerings have business continuity measures for VPUUID ${vpUUID}...`)

    const errorMessages: string[] = results
      .filter(result => !result.legalDocuments.some(legalDocument => legalDocument.type === 'w3id.org/gaia-x/development#BusinessContinuityMeasures'))
      .map(result => {
        this.logger.error(
          `P3.1.16 - Service offering ${result.serviceOfferingId} is missing business continuity measures in its legal documents for VPUUID ${vpUUID}...`
        )
        return `P3.1.16 - Service offering ${result.serviceOfferingId} is missing business continuity measures in its legal documents`
      })

    return {
      conforms: !errorMessages.length,
      results: errorMessages
    }
  }
}
