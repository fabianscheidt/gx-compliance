import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { ValidationFilter } from './common/validation-filter.interface'

/**
 * Implementation of the Gaia-X
 * <a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P1.2.5">P1.2.5 labelling criterion</a>.
 */
export class ServiceOfferingHasAResourceWithAddressFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingHasAResourceWithAddressFilter.name)

  async doFilter(vpUUID: string, _verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Checking that service offerings have a resource with an address for VPUUID ${vpUUID}...`)

    const query = `MATCH (location)
     <-[:_https_w3id_org_gaia_x_development_location_]-(resource)
     <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-(resourceVC)
     -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(resourceType)
     WHERE location.vpID="${vpUUID}" AND resource.vpID="${vpUUID}"
     AND (resourceType.value=~"<https://w3id.org/gaia-x/.*#PhysicalResource>" OR resourceType.value=~"<https://w3id.org/gaia-x/.*#Datacenter>" OR resourceType.value=~"<https://w3id.org/gaia-x/.*#PhysicalInterconnectionPointIdentifier>")
     CALL {
       WITH resource
       MATCH (resource)
       <-[:_https_w3id_org_gaia_x_development_aggregationOfResources_]-(serviceOffering)
       <-[:_https_www_w3_org_2018_credentials_credentialSubject_]-()
       -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(serviceOfferingType)
       WHERE serviceOffering.vpID="${vpUUID}" AND serviceOfferingType.value=~"<https://w3id.org/gaia-x/.*#ServiceOffering>"         
       RETURN DISTINCT serviceOffering
      }       
    RETURN serviceOffering.value as ID,COUNT(*) as anyExisting`
    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))
      let hasAddress = false
      if (results.records.length > 0) {
        hasAddress = results.records
          .flatMap(record => {
            return record.get('anyExisting')?.toNumber() > 0 || false
          })
          .reduce((previousValue, currentValue) => previousValue && currentValue)
      }
      return {
        conforms: hasAddress,
        results: hasAddress ? [] : ['P1.2.5 - No resource with an address provided in the VerifiablePresentation']
      }
    } catch (error) {
      this.logger.error(
        `An error occurred while checking that service offerings have a resource with an address for VPUUID ${vpUUID}. Error: ${error.message}`
      )

      return {
        conforms: false,
        results: [`An error occurred while checking that service offerings have a resource with an address : ${error.message}`]
      }
    } finally {
      await session.close()
    }
  }
}
