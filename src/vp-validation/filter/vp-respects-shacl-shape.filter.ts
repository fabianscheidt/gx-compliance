import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'
import { Summary } from 'prom-client'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { ShaclService } from '../service/shacl.service'
import { ValidationFilter } from './common/validation-filter.interface'

export class VpRespectsShaclShapeFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(VpRespectsShaclShapeFilter.name)

  constructor(private readonly shaclService: ShaclService, private shaclProcessTimeSummary: Summary) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async doFilter(vpUUID: string, verifiablePresentation: VerifiablePresentation, _driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Checking that VPUUID ${vpUUID} respects the SHACL shapes...`)
    const end = this.shaclProcessTimeSummary.startTimer()
    const validationResult = await this.shaclService.verifyShape(verifiablePresentation)
    end({ result: `${validationResult.conforms ? 'valid' : 'invalid'}` })
    return validationResult
  }
}
