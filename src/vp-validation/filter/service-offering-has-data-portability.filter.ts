import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { LegalDocument } from '../../common/model/legal-document'
import { LegalDocumentUtil } from '../../common/utils/legal-document.util'
import { UrlUtils } from '../../common/utils/url.utils'
import { ValidationFilter } from './common/validation-filter.interface'

interface DataPortabilityProperty {
  type: string
  value: string
}

/**
 * Implementation of the following Gaia-X labelling criteria:
 * <ul>
 *   <li><a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P3.1.11">P3.1.11</a></li>
 *   <li><a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P4.1.1">P4.1.1</a></li>
 *   <li><a href="https://gaia-x.gitlab.io/policy-rules-committee/compliance-document/criterions/#P4.1.2">P4.1.2</a></li>
 * </ul>
 */
export class ServiceOfferingHasDataPortabilityFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ServiceOfferingHasDataPortabilityFilter.name)

  async doFilter(vpUUID: string, _verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Checking that service offerings have data portability for VPUUID ${vpUUID}...`)

    const query = `MATCH (:_https_w3id_org_gaia_x_development_ServiceOffering_)<-[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]-(serviceOffering)
        -[:_https_www_w3_org_2018_credentials_credentialSubject_]->(credentialSubject)
      WHERE serviceOffering.vpID="${vpUUID}"
      CALL {
        WITH credentialSubject
        OPTIONAL MATCH (credentialSubject)-[:_https_w3id_org_gaia_x_development_dataPortability_]->(dataPortability)
        RETURN dataPortability
      }
      CALL {
        WITH dataPortability
        MATCH (dataPortability)-[r*1..1]->(property)
        RETURN COLLECT({ type: type(r[0]), value: property.value }) AS properties
      }
      CALL {
        WITH dataPortability
        MATCH (dataPortability)-[:_https_w3id_org_gaia_x_development_legalDocument_]->()
          -[r*1..1]->(property)
        RETURN COLLECT({ type: type(r[0]), value: property.value }) AS legalDocumentProperties
      }
      RETURN serviceOffering.value as serviceOfferingId, dataPortability, properties, legalDocumentProperties`

    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      const errorMessages: string[] = []
      for (const record of results.records) {
        const serviceOfferingId: string = record.get('serviceOfferingId')
        const properties: DataPortabilityProperty[] = record.get('properties')

        if (!record.get('dataPortability')) {
          this.logger.error(`P3.1.11 - Service offering ${serviceOfferingId} is missing data portability for VPUUID ${vpUUID}...`)
          errorMessages.push(`P3.1.11 - Service offering ${serviceOfferingId} is missing data portability`)
        } else {
          let hasMeans = false
          let hasFormats = false
          let hasDocumentations = false
          let hasPricing = false

          for (const property of properties) {
            if (property.type === '_https_w3id_org_gaia_x_development_means_' && property.value?.trim()) {
              hasMeans = true
            }

            if (property.type === '_https_w3id_org_gaia_x_development_formats_' && property.value?.trim()) {
              hasFormats = true
            }

            if (property.type === '_https_w3id_org_gaia_x_development_documentations_' && property.value?.trim()) {
              hasDocumentations = true

              if (!UrlUtils.isValid(property.value)) {
                this.logger.error(
                  `P4.1.1 - Service offering ${serviceOfferingId} has invalid data portability documentation URI '${property.value}' for VPUUID ${vpUUID}...`
                )
                errorMessages.push(
                  `P4.1.1 - Service offering ${serviceOfferingId} has invalid data portability documentation URI '${property.value}'`
                )
              }
            }

            if (property.type === '_https_w3id_org_gaia_x_development_pricing_' && property.value?.trim()) {
              hasPricing = true

              if (!UrlUtils.isValid(property.value)) {
                this.logger.error(
                  `P4.1.1 - Service offering ${serviceOfferingId} has invalid data portability pricing URI '${property.value}' for VPUUID ${vpUUID}...`
                )
                errorMessages.push(`P4.1.1 - Service offering ${serviceOfferingId} has invalid data portability pricing URI '${property.value}'`)
              }
            }
          }

          if (!hasMeans) {
            this.logger.error(`P4.1.1 - Service offering ${serviceOfferingId} is missing data portability means for VPUUID ${vpUUID}...`)
            errorMessages.push(`P4.1.1 - Service offering ${serviceOfferingId} is missing data portability means`)
          }

          if (!hasFormats) {
            this.logger.error(`P4.1.1 - Service offering ${serviceOfferingId} is missing data portability formats for VPUUID ${vpUUID}...`)
            errorMessages.push(`P4.1.1 - Service offering ${serviceOfferingId} is missing data portability formats`)
          }

          if (!hasDocumentations) {
            this.logger.error(`P4.1.1 - Service offering ${serviceOfferingId} is missing data portability documentation for VPUUID ${vpUUID}...`)
            errorMessages.push(`P4.1.1 - Service offering ${serviceOfferingId} is missing data portability documentation`)
          }

          if (!hasPricing) {
            this.logger.error(`P4.1.2 - Service offering ${serviceOfferingId} is missing data portability pricing for VPUUID ${vpUUID}...`)
            errorMessages.push(`P4.1.2 - Service offering ${serviceOfferingId} is missing data portability pricing`)
          }

          if (!record.get('legalDocumentProperties').length) {
            this.logger.error(
              `P4.1.1 - Service offering ${serviceOfferingId} is missing data portability legal documentation for VPUUID ${vpUUID}...`
            )
            errorMessages.push(`P4.1.1 - Service offering ${serviceOfferingId} is missing data portability legal documentation`)
          } else {
            const legalDocument: LegalDocument = LegalDocumentUtil.mapFromProperties(record.get('legalDocumentProperties'))

            try {
              LegalDocumentUtil.isValid(legalDocument)
            } catch (error) {
              this.logger.error(
                `P4.1.1 - Service offering ${serviceOfferingId} data portability legal document is invalid for VPUUID ${vpUUID}: ${error}`
              )
              errorMessages.push(`P4.1.1 - Service offering ${serviceOfferingId} data portability legal document is invalid: ${error}`)
            }
          }
        }
      }

      return {
        conforms: !errorMessages.length,
        results: errorMessages
      }
    } catch (error) {
      this.logger.error(`An error occurred while checking that service offerings have data portability for VPUUID ${vpUUID}. Error: ${error.message}`)

      return {
        conforms: false,
        results: [`An error occurred while checking that service offerings have data portability : ${error.message}`]
      }
    } finally {
      await session.close()
    }
  }
}
