import { Driver } from 'neo4j-driver'

import { ValidationResult } from '../../common/dto'
import { insertObjectInMemGraph, memgraphTestSuite } from '../../tests/memgraph-test-suite'
import { VerifiablePresentationValidationService } from '../service/verifiable-presentation-validation.service'
import { ValidationFilter } from './common/validation-filter.interface'
import vpWithInvalidSubContractorCommunicationMethods from './fixtures/service-offering-has-subcontractor-details-filter/vp-with-invalid-service-offerings.json'
import vpWithValidSubContractorCommunicationMethods from './fixtures/service-offering-has-subcontractor-details-filter/vp-with-valid-service-offerings.json'
import { ServiceOfferingHasSubContractorDetailsFilter } from './service-offering-has-subcontractor-details.filter'

describe('ServiceOfferingHasSubContractorDetailsFilter', () => {
  memgraphTestSuite((driver: Promise<Driver>) => {
    const vpUuid: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    const filter: ValidationFilter = new ServiceOfferingHasSubContractorDetailsFilter()

    it('should return a positive conformity', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithValidSubContractorCommunicationMethods)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(true)
      expect(result.results).toHaveLength(0)
    })

    it('should return a negative conformity with a reason when some SubContractors are missing attributes', async () => {
      await insertObjectInMemGraph(await driver, vpUuid, vpWithInvalidSubContractorCommunicationMethods)

      const result: ValidationResult = await filter.doFilter(vpUuid, null, await driver)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual([
        `P1.2.6 - P1.2.7 - Service offering <https://example.org/invalid-service-offering.json> has no subcontractors`,
        `P1.2.7 - Subcontractor <https://example.org/contractor-no-legal-name.json> in service offering <https://example.org/invalid-no-subcontractors-communication-method-service-offering.json> is missing a legal name`,
        `P1.2.6 - Subcontractor <https://example.org/contractor-no-information-documents.json> in service offering <https://example.org/invalid-no-subcontractors-communication-method-service-offering.json> is missing information documents`,
        `P1.2.6 - Subcontractor <https://example.org/contractor-no-communication-method.json> in service offering <https://example.org/invalid-no-subcontractors-communication-method-service-offering.json> is missing a communication method`,
        `P1.2.7 - Subcontractor <https://example.org/contractor-no-applicable-jurisdiction.json> in service offering <https://example.org/invalid-no-subcontractors-communication-method-service-offering.json> is missing an applicable jurisdiction`
      ])
    })

    it('should return a negative conformity with a reason when the query fails', async () => {
      const driverMock: Driver = {
        session: () => ({
          executeRead: jest.fn().mockRejectedValue(new Error('Test error')),
          close: jest.fn()
        })
      } as unknown as Driver

      const result: ValidationResult = await filter.doFilter(vpUuid, null, driverMock)

      expect(result.conforms).toBe(false)
      expect(result.results).toEqual(['An error occurred while checking subcontractor details: Test error'])
    })
  })
})
