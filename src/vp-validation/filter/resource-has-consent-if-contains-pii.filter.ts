import { Logger } from '@nestjs/common'

import { Driver } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { ValidationFilter } from './common/validation-filter.interface'

export class ResourceHasConsentIfContainsPiiFilter implements ValidationFilter {
  private readonly logger: Logger = new Logger(ResourceHasConsentIfContainsPiiFilter.name)

  async doFilter(vpUUID: string, _verifiablePresentation: VerifiablePresentation, driver: Driver): Promise<ValidationResult> {
    this.logger.debug(`Checking that resources have consent when containing PII for VPUUID ${vpUUID}...`)

    const query = `OPTIONAL MATCH (resource)-[:_https_w3id_org_gaia_x_development_containsPII_]->(pii)
      WHERE resource.vpID="${vpUUID}" 
        AND pii.value='true'
          CALL {
            WITH resource
            OPTIONAL MATCH (resource)-[:_https_w3id_org_gaia_x_development_consent_]->(consent)
              -[:_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_]->(:_https_w3id_org_gaia_x_development_Consent_)
            RETURN consent
          }
      RETURN pii,consent`
    const session = driver.session()
    try {
      const results = await session.executeRead(tx => tx.run(query))

      this.logger.debug(`Found ${results.records.length} resources containing PII for VPUUID ${vpUUID}...`)
      const hasPIIAndConsentOrNoPII = results.records
        .flatMap(record => {
          return (!!record.get('pii') && !!record.get('consent')) || !record.get('pii')
        })
        .reduce((previousValue, currentValue) => previousValue && currentValue)

      return {
        conforms: hasPIIAndConsentOrNoPII,
        results: hasPIIAndConsentOrNoPII ? [] : ['Resource contains PII without Consent']
      }
    } catch (error) {
      this.logger.error(`An error occurred while checking for PII and consent in resources for VPUUID ${vpUUID}. Error: ${error.message}`)

      return {
        conforms: false,
        results: [`An error occurred while checking for PII and consent in resources: ${error.message}`]
      }
    } finally {
      await session.close()
    }
  }
}
