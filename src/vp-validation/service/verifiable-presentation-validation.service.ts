import { Injectable, Logger, Scope } from '@nestjs/common'

import { InjectMetric } from '@willsoto/nestjs-prometheus'
import jsonld from 'jsonld'
import neo4j from 'neo4j-driver'
import * as process from 'node:process'
import { Summary } from 'prom-client'
import { v4 as uuidv4 } from 'uuid'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { ConformityLevelEnum } from '../../common/enum/conformity-level.enum'
import { mergeResults } from '../../common/utils/merge-results'
import { RdfGraphUtils } from '../../common/utils/rdf-graph.utils'
import {
  DataResourceIssuerMatchesProducerIssuerFilter,
  IssuersHaveTermsAndConditionsFilter,
  LegalDocumentHasInvolvedParties,
  RegistrationNumbersAreFromTrustedIssuersFilter,
  ResourceHasConsentIfContainsPiiFilter,
  ServiceOfferingHasAResourceWithAddressFilter,
  ServiceOfferingHasAccessControlManagementFilter,
  ServiceOfferingHasAssetsManagementFilter,
  ServiceOfferingHasBusinessContinuityMeasuresFilter,
  ServiceOfferingHasChangeAndConfigurationManagementFilter,
  ServiceOfferingHasComplianceAssuranceFilter,
  ServiceOfferingHasCryptographicSecurityStandards,
  ServiceOfferingHasCustomerAuditingRightsFilter,
  ServiceOfferingHasCustomerDataAccessTermsFilter,
  ServiceOfferingHasCustomerInstructionsFilter,
  ServiceOfferingHasDataPortabilityFilter,
  ServiceOfferingHasDataUsageRightsForPartiesFilter,
  ServiceOfferingHasDescribedDataTransfersFilter,
  ServiceOfferingHasDevelopmentCycleSecurityFilter,
  ServiceOfferingHasDocumentedChangeProceduresFilter,
  ServiceOfferingHasEmployeeResponsibilitiesFilter,
  ServiceOfferingHasGovernmentInvestigationManagementFilter,
  ServiceOfferingHasInformationSecurityOrganizationFilter,
  ServiceOfferingHasInformationSecurityPoliciesFilter,
  ServiceOfferingHasInformationSecurityRiskManagementFilter,
  ServiceOfferingHasLegallyBindingActFilter,
  ServiceOfferingHasOperationalSecurityFilter,
  ServiceOfferingHasPhysicalSecurityFilter,
  ServiceOfferingHasProcurementManagementSecurityFilter,
  ServiceOfferingHasProductSecurityFilter,
  ServiceOfferingHasProviderContactInformationFilter,
  ServiceOfferingHasRequiredMeasuresFilter,
  ServiceOfferingHasRoleAndResponsibilitiesFilter,
  ServiceOfferingHasSecurityIncidentManagementFilter,
  ServiceOfferingHasSubContractorDetailsFilter,
  ServiceOfferingHasUserDocumentationMaintenanceFilter,
  ServiceOfferingHaveDataProtectionAndServiceAgreementFilter,
  ServiceOfferingIsOperatedByGaiaXParticipantFilter,
  ServiceOfferingIssuerMatchesProviderIssuerFilter,
  ValidationFilter,
  VerifiableCredentialsAreNotExpiredFilter,
  VpRespectsShaclShapeFilter
} from '../filter'
import { ShaclService } from './shacl.service'
import { TrustedNotaryIssuerService } from './trusted-notary-issuer.service'

@Injectable({ scope: Scope.REQUEST })
export class VerifiablePresentationValidationService {
  private readonly serviceOfferingFilters: Record<ConformityLevelEnum, ValidationFilter[]>
  // TODO replace this by the use of ConfigService
  private readonly _driver = neo4j.driver(process.env.dburl || 'bolt://localhost:7687')
  private readonly logger: Logger = new Logger(VerifiablePresentationValidationService.name)

  constructor(
    @InjectMetric('shacl_process_time') protected shaclProcessTimeSummary: Summary,
    private readonly trustedNotaryIssuerService: TrustedNotaryIssuerService,
    private readonly shaclService: ShaclService
  ) {
    const verifiableCredentialsAreNotExpiredFilter: ValidationFilter = new VerifiableCredentialsAreNotExpiredFilter()
    const vpRespectsShaclShapeFilter: ValidationFilter = new VpRespectsShaclShapeFilter(this.shaclService, shaclProcessTimeSummary)
    const dataResourceIssuerMatchesProducerIssuerFilter: ValidationFilter = new DataResourceIssuerMatchesProducerIssuerFilter()
    const issuersHaveTermsAndConditionsFilter: ValidationFilter = new IssuersHaveTermsAndConditionsFilter(this.trustedNotaryIssuerService)
    const registrationNumbersAreFromTrustedIssuersFilter: ValidationFilter = new RegistrationNumbersAreFromTrustedIssuersFilter(
      this.trustedNotaryIssuerService
    )
    const resourceHasConsentIfContainsPiiFilter: ValidationFilter = new ResourceHasConsentIfContainsPiiFilter()
    const serviceOfferingIssuerMatchesProviderIssuerFilter: ValidationFilter = new ServiceOfferingIssuerMatchesProviderIssuerFilter()
    const serviceOfferingHasAResourceWithAddressFilter: ValidationFilter = new ServiceOfferingHasAResourceWithAddressFilter()
    const serviceOfferingHasLegallyBindingActFilter: ValidationFilter = new ServiceOfferingHasLegallyBindingActFilter()
    const serviceOfferingHasDataUsageRightsForPartiesFilter: ValidationFilter = new ServiceOfferingHasDataUsageRightsForPartiesFilter()
    const legalDocumentHasInvolvedParties: ValidationFilter = new LegalDocumentHasInvolvedParties()
    const serviceOfferingHasRequiredMeasuresFilter: ValidationFilter = new ServiceOfferingHasRequiredMeasuresFilter()
    const serviceOfferingHasDocumentedChangeProceduresFilter: ValidationFilter = new ServiceOfferingHasDocumentedChangeProceduresFilter()
    const serviceOfferingHasRoleAndResponsibilitiesFilter: ValidationFilter = new ServiceOfferingHasRoleAndResponsibilitiesFilter()
    const serviceOfferingHasProviderContactInformationFilter: ValidationFilter = new ServiceOfferingHasProviderContactInformationFilter()
    const serviceOfferingHasCustomerInstructionsFilter: ValidationFilter = new ServiceOfferingHasCustomerInstructionsFilter()
    const serviceOfferingIsOperatedByGaiaXParticipantFilter: ValidationFilter = new ServiceOfferingIsOperatedByGaiaXParticipantFilter()
    const serviceOfferingHasDescribedDataTransfersFilter: ValidationFilter = new ServiceOfferingHasDescribedDataTransfersFilter()
    const serviceOfferingHasCustomerAuditingRightsFilter: ValidationFilter = new ServiceOfferingHasCustomerAuditingRightsFilter()
    const serviceOfferingHasAssetsManagementFilter: ValidationFilter = new ServiceOfferingHasAssetsManagementFilter()
    const serviceOfferingHasEmployeeResponsibilitiesFilter: ValidationFilter = new ServiceOfferingHasEmployeeResponsibilitiesFilter()
    const serviceOfferingHasInformationSecurityPoliciesFilter: ValidationFilter = new ServiceOfferingHasInformationSecurityPoliciesFilter()
    const serviceOfferingHasOperationalSecurityFilter: ValidationFilter = new ServiceOfferingHasOperationalSecurityFilter()
    const serviceOfferingHasAccessControlManagementFilter: ValidationFilter = new ServiceOfferingHasAccessControlManagementFilter()
    const serviceOfferingHasInformationSecurityOrganizationFilter: ValidationFilter = new ServiceOfferingHasInformationSecurityOrganizationFilter()
    const serviceOfferingHasInformationSecurityRiskManagementFilter: ValidationFilter =
      new ServiceOfferingHasInformationSecurityRiskManagementFilter()
    const serviceOfferingHasPhysicalSecurityFilter: ValidationFilter = new ServiceOfferingHasPhysicalSecurityFilter()
    const serviceOfferingHasCryptographicSecurityStandards: ValidationFilter = new ServiceOfferingHasCryptographicSecurityStandards()
    const serviceOfferingHasDataPortabilityFilter: ValidationFilter = new ServiceOfferingHasDataPortabilityFilter()
    const serviceOfferingHasDevelopmentCycleSecurityFilter: ValidationFilter = new ServiceOfferingHasDevelopmentCycleSecurityFilter()
    const serviceOfferingHasProcurementManagementSecurityFilter: ValidationFilter = new ServiceOfferingHasProcurementManagementSecurityFilter()
    const serviceOfferingHasSecurityIncidentManagementFilter: ValidationFilter = new ServiceOfferingHasSecurityIncidentManagementFilter()
    const serviceOfferingHasBusinessContinuityMeasuresFilter: ValidationFilter = new ServiceOfferingHasBusinessContinuityMeasuresFilter()
    const serviceOfferingHasComplianceAssuranceFilter: ValidationFilter = new ServiceOfferingHasComplianceAssuranceFilter()
    const serviceOfferingHasUserDocumentationMaintenanceFilter: ValidationFilter = new ServiceOfferingHasUserDocumentationMaintenanceFilter()
    const serviceOfferingHasGovernmentInvestigationManagementFilter: ValidationFilter =
      new ServiceOfferingHasGovernmentInvestigationManagementFilter()
    const serviceOfferingHasProductSecurityFilter: ValidationFilter = new ServiceOfferingHasProductSecurityFilter()
    const serviceOfferingHasChangeAndConfigurationManagementFilter: ValidationFilter = new ServiceOfferingHasChangeAndConfigurationManagementFilter()
    const serviceOfferingHasCustomerDataAccessTermsFilter: ValidationFilter = new ServiceOfferingHasCustomerDataAccessTermsFilter()
    const serviceOfferingHaveDataProtectionAndServiceAgreementFilter: ValidationFilter =
      new ServiceOfferingHaveDataProtectionAndServiceAgreementFilter()
    const serviceOfferingHasSubContractorDetailsFilter: ValidationFilter = new ServiceOfferingHasSubContractorDetailsFilter()

    this.serviceOfferingFilters = {
      [ConformityLevelEnum.BASIC_CONFORMITY]: [
        verifiableCredentialsAreNotExpiredFilter,
        vpRespectsShaclShapeFilter,
        dataResourceIssuerMatchesProducerIssuerFilter,
        issuersHaveTermsAndConditionsFilter,
        registrationNumbersAreFromTrustedIssuersFilter,
        resourceHasConsentIfContainsPiiFilter,
        serviceOfferingIssuerMatchesProviderIssuerFilter,
        serviceOfferingHasAResourceWithAddressFilter,
        serviceOfferingHasLegallyBindingActFilter,
        serviceOfferingHasDataUsageRightsForPartiesFilter,
        serviceOfferingHasDocumentedChangeProceduresFilter,
        legalDocumentHasInvolvedParties,
        serviceOfferingHasRequiredMeasuresFilter,
        serviceOfferingHasProviderContactInformationFilter,
        serviceOfferingHasRoleAndResponsibilitiesFilter,
        serviceOfferingHasAccessControlManagementFilter,
        serviceOfferingHasCustomerInstructionsFilter,
        serviceOfferingHasOperationalSecurityFilter,
        serviceOfferingIsOperatedByGaiaXParticipantFilter,
        serviceOfferingHasCustomerAuditingRightsFilter,
        serviceOfferingHasAssetsManagementFilter,
        serviceOfferingHasInformationSecurityOrganizationFilter,
        serviceOfferingHasEmployeeResponsibilitiesFilter,
        serviceOfferingHasInformationSecurityRiskManagementFilter,
        serviceOfferingHasInformationSecurityPoliciesFilter,
        serviceOfferingHasPhysicalSecurityFilter,
        serviceOfferingHasCryptographicSecurityStandards,
        serviceOfferingHasDataPortabilityFilter,
        serviceOfferingHasDevelopmentCycleSecurityFilter,
        serviceOfferingHasProcurementManagementSecurityFilter,
        serviceOfferingHasSecurityIncidentManagementFilter,
        serviceOfferingHasBusinessContinuityMeasuresFilter,
        serviceOfferingHasComplianceAssuranceFilter,
        serviceOfferingHasUserDocumentationMaintenanceFilter,
        serviceOfferingHasGovernmentInvestigationManagementFilter,
        serviceOfferingHasProductSecurityFilter,
        serviceOfferingHasChangeAndConfigurationManagementFilter,
        serviceOfferingHasCustomerDataAccessTermsFilter,
        serviceOfferingHasSubContractorDetailsFilter
      ],
      [ConformityLevelEnum.LABEL_LEVEL_1]: [
        verifiableCredentialsAreNotExpiredFilter,
        vpRespectsShaclShapeFilter,
        dataResourceIssuerMatchesProducerIssuerFilter,
        issuersHaveTermsAndConditionsFilter,
        registrationNumbersAreFromTrustedIssuersFilter,
        resourceHasConsentIfContainsPiiFilter,
        serviceOfferingIssuerMatchesProviderIssuerFilter,
        serviceOfferingHasAResourceWithAddressFilter,
        serviceOfferingHasLegallyBindingActFilter,
        serviceOfferingHasDataUsageRightsForPartiesFilter,
        legalDocumentHasInvolvedParties,
        serviceOfferingHasRequiredMeasuresFilter,
        serviceOfferingHasDocumentedChangeProceduresFilter,
        serviceOfferingHasProviderContactInformationFilter,
        serviceOfferingHasRoleAndResponsibilitiesFilter,
        serviceOfferingHasAccessControlManagementFilter,
        serviceOfferingHasCustomerInstructionsFilter,
        serviceOfferingHasOperationalSecurityFilter,
        serviceOfferingIsOperatedByGaiaXParticipantFilter,
        serviceOfferingHasCustomerAuditingRightsFilter,
        serviceOfferingHasAssetsManagementFilter,
        serviceOfferingHasInformationSecurityOrganizationFilter,
        serviceOfferingHasEmployeeResponsibilitiesFilter,
        serviceOfferingHasInformationSecurityRiskManagementFilter,
        serviceOfferingHasDescribedDataTransfersFilter,
        serviceOfferingHasInformationSecurityPoliciesFilter,
        serviceOfferingHasPhysicalSecurityFilter,
        serviceOfferingHasCryptographicSecurityStandards,
        serviceOfferingHasDataPortabilityFilter,
        serviceOfferingHasDevelopmentCycleSecurityFilter,
        serviceOfferingHasProcurementManagementSecurityFilter,
        serviceOfferingHasSecurityIncidentManagementFilter,
        serviceOfferingHasBusinessContinuityMeasuresFilter,
        serviceOfferingHasComplianceAssuranceFilter,
        serviceOfferingHasUserDocumentationMaintenanceFilter,
        serviceOfferingHasGovernmentInvestigationManagementFilter,
        serviceOfferingHasProductSecurityFilter,
        serviceOfferingHasChangeAndConfigurationManagementFilter,
        serviceOfferingHasCustomerDataAccessTermsFilter,
        serviceOfferingHaveDataProtectionAndServiceAgreementFilter,
        serviceOfferingHasSubContractorDetailsFilter
      ],
      [ConformityLevelEnum.LABEL_LEVEL_2]: [
        verifiableCredentialsAreNotExpiredFilter,
        vpRespectsShaclShapeFilter,
        dataResourceIssuerMatchesProducerIssuerFilter,
        issuersHaveTermsAndConditionsFilter,
        registrationNumbersAreFromTrustedIssuersFilter,
        resourceHasConsentIfContainsPiiFilter,
        serviceOfferingIssuerMatchesProviderIssuerFilter,
        serviceOfferingHasAResourceWithAddressFilter,
        serviceOfferingHasLegallyBindingActFilter,
        serviceOfferingHasDataUsageRightsForPartiesFilter,
        legalDocumentHasInvolvedParties,
        serviceOfferingHasRequiredMeasuresFilter,
        serviceOfferingHasDocumentedChangeProceduresFilter,
        serviceOfferingHasProviderContactInformationFilter,
        serviceOfferingHasRoleAndResponsibilitiesFilter,
        serviceOfferingHasAccessControlManagementFilter,
        serviceOfferingHasCustomerInstructionsFilter,
        serviceOfferingHasOperationalSecurityFilter,
        serviceOfferingIsOperatedByGaiaXParticipantFilter,
        serviceOfferingHasCustomerAuditingRightsFilter,
        serviceOfferingHasAssetsManagementFilter,
        serviceOfferingHasInformationSecurityOrganizationFilter,
        serviceOfferingHasEmployeeResponsibilitiesFilter,
        serviceOfferingHasInformationSecurityRiskManagementFilter,
        serviceOfferingHasDescribedDataTransfersFilter,
        serviceOfferingHasInformationSecurityPoliciesFilter,
        serviceOfferingHasPhysicalSecurityFilter,
        serviceOfferingHasCryptographicSecurityStandards,
        serviceOfferingHasDataPortabilityFilter,
        serviceOfferingHasDevelopmentCycleSecurityFilter,
        serviceOfferingHasProcurementManagementSecurityFilter,
        serviceOfferingHasSecurityIncidentManagementFilter,
        serviceOfferingHasBusinessContinuityMeasuresFilter,
        serviceOfferingHasComplianceAssuranceFilter,
        serviceOfferingHasUserDocumentationMaintenanceFilter,
        serviceOfferingHasGovernmentInvestigationManagementFilter,
        serviceOfferingHasProductSecurityFilter,
        serviceOfferingHasChangeAndConfigurationManagementFilter,
        serviceOfferingHasCustomerDataAccessTermsFilter,
        serviceOfferingHaveDataProtectionAndServiceAgreementFilter,
        serviceOfferingHasSubContractorDetailsFilter
      ],
      [ConformityLevelEnum.LABEL_LEVEL_3]: [
        verifiableCredentialsAreNotExpiredFilter,
        vpRespectsShaclShapeFilter,
        dataResourceIssuerMatchesProducerIssuerFilter,
        issuersHaveTermsAndConditionsFilter,
        registrationNumbersAreFromTrustedIssuersFilter,
        resourceHasConsentIfContainsPiiFilter,
        serviceOfferingIssuerMatchesProviderIssuerFilter,
        serviceOfferingHasAResourceWithAddressFilter,
        serviceOfferingHasLegallyBindingActFilter,
        serviceOfferingHasDataUsageRightsForPartiesFilter,
        legalDocumentHasInvolvedParties,
        serviceOfferingHasRequiredMeasuresFilter,
        serviceOfferingHasDocumentedChangeProceduresFilter,
        serviceOfferingHasProviderContactInformationFilter,
        serviceOfferingHasRoleAndResponsibilitiesFilter,
        serviceOfferingHasAccessControlManagementFilter,
        serviceOfferingHasCustomerInstructionsFilter,
        serviceOfferingHasOperationalSecurityFilter,
        serviceOfferingIsOperatedByGaiaXParticipantFilter,
        serviceOfferingHasCustomerAuditingRightsFilter,
        serviceOfferingHasAssetsManagementFilter,
        serviceOfferingHasInformationSecurityOrganizationFilter,
        serviceOfferingHasEmployeeResponsibilitiesFilter,
        serviceOfferingHasInformationSecurityRiskManagementFilter,
        serviceOfferingHasInformationSecurityPoliciesFilter,
        serviceOfferingHasPhysicalSecurityFilter,
        serviceOfferingHasCryptographicSecurityStandards,
        serviceOfferingHasDataPortabilityFilter,
        serviceOfferingHasDevelopmentCycleSecurityFilter,
        serviceOfferingHasProcurementManagementSecurityFilter,
        serviceOfferingHasSecurityIncidentManagementFilter,
        serviceOfferingHasBusinessContinuityMeasuresFilter,
        serviceOfferingHasComplianceAssuranceFilter,
        serviceOfferingHasUserDocumentationMaintenanceFilter,
        serviceOfferingHasGovernmentInvestigationManagementFilter,
        serviceOfferingHasProductSecurityFilter,
        serviceOfferingHasChangeAndConfigurationManagementFilter,
        serviceOfferingHasCustomerDataAccessTermsFilter,
        serviceOfferingHaveDataProtectionAndServiceAgreementFilter,
        serviceOfferingHasSubContractorDetailsFilter
      ]
    }
  }

  async validate(verifiablePresentation: VerifiablePresentation, conformityLevel: ConformityLevelEnum): Promise<ValidationResult> {
    const vpUUID: string = VerifiablePresentationValidationService.getUUIDStartingWithALetter()
    await this.insertVpInMemGraph(vpUUID, verifiablePresentation)

    const filtersPromise = this.serviceOfferingFilters[conformityLevel].map(filter => filter.doFilter(vpUUID, verifiablePresentation, this._driver))
    const validationResults: ValidationResult[] = await Promise.all(filtersPromise)

    await this.cleanupMemGraph(vpUUID)

    return mergeResults(...validationResults)
  }

  static getUUIDStartingWithALetter() {
    let uuid = uuidv4()
    while (!isNaN(uuid[0])) {
      uuid = uuidv4()
    }
    return uuid
  }

  private async insertVpInMemGraph(vpUUID: string, verifiablePresentation: VerifiablePresentation): Promise<void> {
    const quads: string = await jsonld.toRDF(verifiablePresentation, { format: 'application/n-quads' })
    const queries: string[] = RdfGraphUtils.quadsToQueries(vpUUID, quads)

    const session = this._driver.session()
    try {
      for (const query of queries) {
        try {
          await session.executeWrite(tx => tx.run(query))
        } catch (error) {
          this.logger.log(query)
        }
      }
    } catch (error) {
      this.logger.log(`An error occurred while inserting the verifiable presentation in MemGraph: ${error}`)
    } finally {
      await session.close()
    }
  }

  private async cleanupMemGraph(VPUUID: any) {
    const session = this._driver.session()
    try {
      await session.executeWrite(tx => tx.run(`MATCH (n) WHERE n.vpID="${VPUUID}" DETACH DELETE n`))
    } catch (e) {
      this.logger.warn(`An error occurred while removing VP ${VPUUID} from DB`, e)
    } finally {
      await session.close()
    }
  }
}
