import { ConflictException, Inject, Injectable, Logger, Scope } from '@nestjs/common'

import { DocumentLoader } from '@gaia-x/json-web-signature-2020'
import { exec } from 'child_process'
import jsonld from 'jsonld'
import * as fs from 'node:fs'
import { mkdtemp } from 'node:fs/promises'
import { tmpdir } from 'node:os'
import * as path from 'node:path'
import { join } from 'path'
import DatasetExt from 'rdf-ext/lib/Dataset'
import { promisify } from 'util'

import { ValidationResult } from '../../common/dto'
import { RegistryService } from './registry.service'

const cache: Map<string, DatasetExt> = new Map()
const promiseExec = promisify(exec)

@Injectable({ scope: Scope.REQUEST })
export class ShaclService {
  private readonly logger = new Logger(ShaclService.name)

  constructor(@Inject('documentLoader') private readonly documentLoader: DocumentLoader, private readonly registryService: RegistryService) {}

  async loadShaclFromUrl(contextUrl: string): Promise<string> {
    try {
      return await this.registryService.loadTurtleShapesFromUrl(contextUrl)
    } catch (error) {
      this.logger.error(`${error}, Url used to fetch shapes: ${contextUrl}`)
      throw new ConflictException(error)
    }
  }

  public async verifyShape(verifiablePresentation: any): Promise<ValidationResult> {
    try {
      const directory = await mkdtemp(join(tmpdir(), 'shaclValidation-'))
      const credentialSubjects = this.extractCredentialSubjectsFromVerifiablePresentation(verifiablePresentation)
      const vcs = {
        '@context': this.extractContextsFromSubjects(credentialSubjects),
        '@graph': credentialSubjects
      }
      const gaiaXContext = this.getShapesUrlFromContext(verifiablePresentation)
      try {
        const quads = await jsonld.canonize(vcs, {
          format: 'application/n-quads',
          documentLoader: this.documentLoader
        })
        fs.writeFileSync(directory + '/data.n3', quads)
      } catch (error) {
        throw new ConflictException('Unable to canonize your VerifiablePresentation ' + error.message)
      }

      if (!this.isCached(gaiaXContext)) {
        cache[gaiaXContext] = await this.loadShaclFromUrl(gaiaXContext)
      }
      fs.writeFileSync(directory + '/shape.ttl', cache[gaiaXContext])

      let res = { stdout: '', stderr: '' }
      try {
        res = await promiseExec(
          `${path.join(__dirname, '../../../tools/jena/bin/')}shacl validate --data ${directory}/data.n3 --shapes ${directory}/shape.ttl`
        )
        this.logger.log(res)
      } catch (error) {
        this.logger.error(res)
        return {
          conforms: false,
          results: [error]
        }
      }

      if (/sh:conforms *true/.test(res.stdout)) {
        return new ValidationResult()
      } else {
        return {
          conforms: false,
          results: this.formatJenaShacl(res.stdout)
        }
      }
    } catch (e) {
      this.logger.error(e)
      return {
        conforms: false,
        results: [e.message]
      }
    }
  }

  private isCached(type: string): boolean {
    let cached = false
    if (cache[type] && cache[type].shape) {
      cached = true
    }
    return cached
  }

  private getShapesUrlFromContext(verifiablePresentation: any): string {
    return verifiablePresentation?.verifiableCredential
      .flatMap(cred => cred['@context'])
      .flat(Infinity)
      .find(ctx => ctx.indexOf('https://w3id.org/gaia-x/') > -1)
  }

  /**
   * This is used to extract credential subjects from verifiable credentials contained in the verifiablePresentation
   * Can be removed as soon as the shapes are able to target CredentialSubjects directly and not specific target classes
   * @param verifiablePresentation
   */
  public extractCredentialSubjectsFromVerifiablePresentation(verifiablePresentation: any): any[] {
    if (!Array.isArray(verifiablePresentation['verifiableCredential'])) {
      return this.extractCredentialSubjectsFromVerifiableCredential(verifiablePresentation['verifiableCredential'])
    }
    const credentialSubjects = []
    for (const vc of verifiablePresentation['verifiableCredential']) {
      credentialSubjects.push(...this.extractCredentialSubjectsFromVerifiableCredential(vc))
    }
    return credentialSubjects
  }

  private extractCredentialSubjectsFromVerifiableCredential(verifiableCredential: any): any[] {
    if (!Array.isArray(verifiableCredential['credentialSubject'])) {
      return [
        this.mapCredentialSubjectIntoUsableJSONLD(
          verifiableCredential['credentialSubject'],
          verifiableCredential['@context'],
          verifiableCredential['type'] ?? verifiableCredential['@type']
        )
      ]
    }
    const mappedCS = []
    for (const cs of verifiableCredential['credentialSubject']) {
      mappedCS.push(
        this.mapCredentialSubjectIntoUsableJSONLD(cs, verifiableCredential['@context'], verifiableCredential['type'] ?? verifiableCredential['@type'])
      )
    }
    return mappedCS
  }

  public formatJenaShacl(results: string) {
    const formattedShaclErrors = []
    const regex = /(sh:resultPath|focusNode|resultMessage|sourceConstraintComponent|sourceShape|value) *"?(.*?)"?[;|\n]/gm
    const shaclErrors = results.matchAll(regex)
    let countForNewError = 0
    let errorMessage = 'An error has occured while checking the shape of your input '
    for (const error of shaclErrors) {
      if (countForNewError === 6) {
        formattedShaclErrors.push(errorMessage)
        countForNewError = 0
        errorMessage = 'An error has occured while checking the shape of your input '
      }
      errorMessage += ` ${error[1]}:${error[2]} `
      countForNewError++
    }
    formattedShaclErrors.push(errorMessage)
    return formattedShaclErrors
  }

  private extractContextsFromSubjects(credentialSubjects: any[]): string[] {
    const contexts = new Set<string>()
    credentialSubjects
      .map((cs: Array<string>) => {
        return cs['@context']
      })
      .flat()
      .forEach(context => {
        contexts.add(context)
      })
    return Array.from(contexts)
  }

  private mapCredentialSubjectIntoUsableJSONLD(credentialSubject: any, contexts: string[], types: any[]) {
    const mappedCS = {
      ...credentialSubject,
      '@context': [...contexts],
      type: [...types]
    }
    if (!!credentialSubject['type'] && Array.isArray(credentialSubject['type'])) {
      mappedCS.type.push(...credentialSubject['type'])
    } else if (!!credentialSubject['type']) {
      mappedCS.type.push(credentialSubject['type'])
    }
    if (!!credentialSubject['@type'] && Array.isArray(credentialSubject['@type'])) {
      mappedCS.type.push(...credentialSubject['@type'])
    } else if (!!credentialSubject['@type']) {
      mappedCS.type.push(credentialSubject['@type'])
    }
    return mappedCS
  }
}
