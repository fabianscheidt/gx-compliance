import jsonld from 'jsonld'

import { RegistryService } from './registry.service'
import { ShaclService } from './shacl.service'

describe('ShaclService', () => {
  const registryServiceMock: RegistryService = {
    loadTurtleShapesFromUrl: jest.fn(),
    isValidCertificateChain: jest.fn()
  } as unknown as RegistryService

  const service: ShaclService = new ShaclService(jsonld.documentLoaders.node(), registryServiceMock)
  it('should be defined', () => {
    expect(service).toBeDefined()
  })
  it('should load shapes from registry', async () => {
    jest.spyOn(registryServiceMock, 'loadTurtleShapesFromUrl').mockReturnValue(Promise.resolve('test string'))
    const shapes = await service.loadShaclFromUrl('https://example.org')
    expect(shapes).toBeDefined()
    expect(shapes).toBe('test string')
  })
  it('should throw an exception when shapes cannot be loaded from registry', () => {
    jest.spyOn(registryServiceMock, 'loadTurtleShapesFromUrl').mockReturnValue(Promise.reject('error string'))
    expect(service.loadShaclFromUrl('https://example.org')).rejects.toThrowError(/error string/)
  })
  it('should be able to parse SHACL results', () => {
    const results = service.formatJenaShacl(shaclResult)
    expect(results).toEqual([
      'An error has occured while checking the shape of your input  focusNode:<https://trusted-issuer.com/so.json#cs>  resultMessage:Or at focusNode <https://trusted-issuer.com/data.json#cs>  sh:resultPath:gx:aggregationOfResources  sourceConstraintComponent:sh:OrConstraintComponent  sourceShape:[]   value:<https://trusted-issuer.com/data.json#cs> ',
      'An error has occured while checking the shape of your input  focusNode:<https://trusted-issuer.com/so.json#cs>  resultMessage:Closed[https://schema.org/description, https://w3id.org/gaia-x/development#dataAccountExport, https://w3id.org/gaia-x/development#dataProtectionRegime, https://w3id.org/gaia-x/development#serviceScope, https://w3id.org/gaia-x/development#providerContactInformation, https://w3id.org/gaia-x/development#legalDocuments, https://w3id.org/gaia-x/development#possiblePersonalDataTransfers, https://w3id.org/gaia-x/development#keyword, https://w3id.org/gaia-x/development#hostedOn, https://w3id.org/gaia-x/development#endpoint, https://w3id.org/gaia-x/development#provisionType, https://w3id.org/gaia-x/development#cryptographicSecurityStandards, https://w3id.org/gaia-x/development#aggregationOfResources, https://schema.org/name, https://w3id.org/gaia-x/development#dependsOn, https://w3id.org/gaia-x/development#subContractors, https://w3id.org/gaia-x/development#requiredMeasures, https://w3id.org/gaia-x/development#serviceOfferingTermsAndConditions, https://w3id.org/gaia-x/development#providedBy, https://w3id.org/gaia-x/development#customerInstructions, https://w3id.org/gaia-x/development#dataPortability, https://w3id.org/gaia-x/development#servicePolicy][http://www.w3.org/1999/02/22-rdf-syntax-ns#type] Property = <https://w3id.org/gaia-x/development#legallyBindingActs> : Object = _:Bc7b3a2a6b1647355895988ef482c5690  sh:resultPath:gx:legallyBindingActs  sourceConstraintComponent:sh:ClosedConstraintComponent  sourceShape:gx:ServiceOfferingShape  value:[]  ',
      'An error has occured while checking the shape of your input  focusNode:<https://trusted-issuer.com/so.json#cs>  resultMessage:Closed[https://schema.org/description, https://w3id.org/gaia-x/development#dataAccountExport, https://w3id.org/gaia-x/development#dataProtectionRegime, https://w3id.org/gaia-x/development#serviceScope, https://w3id.org/gaia-x/development#providerContactInformation, https://w3id.org/gaia-x/development#legalDocuments, https://w3id.org/gaia-x/development#possiblePersonalDataTransfers, https://w3id.org/gaia-x/development#keyword, https://w3id.org/gaia-x/development#hostedOn, https://w3id.org/gaia-x/development#endpoint, https://w3id.org/gaia-x/development#provisionType, https://w3id.org/gaia-x/development#cryptographicSecurityStandards, https://w3id.org/gaia-x/development#aggregationOfResources, https://schema.org/name, https://w3id.org/gaia-x/development#dependsOn, https://w3id.org/gaia-x/development#subContractors, https://w3id.org/gaia-x/development#requiredMeasures, https://w3id.org/gaia-x/development#serviceOfferingTermsAndConditions, https://w3id.org/gaia-x/development#providedBy, https://w3id.org/gaia-x/development#customerInstructions, https://w3id.org/gaia-x/development#dataPortability, https://w3id.org/gaia-x/development#servicePolicy][http://www.w3.org/1999/02/22-rdf-syntax-ns#type] Property = <https://w3id.org/gaia-x/development#legallyBindingActs> : Object = _:B1e8fad3891b1c2ca33607f2a4bf06d18  sh:resultPath:gx:legallyBindingActs  sourceConstraintComponent:sh:ClosedConstraintComponent  sourceShape:gx:ServiceOfferingShape  value:[]  '
    ])
  })
  describe('CredentialSubject retrieval', () => {
    it('should retrieve credentialSubjects from VerifiablePresentation with multiple credentials', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(JSON.parse(vpArrayOfCredsSingleSubject))
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(3)
      expect(subjects[0]['type']).toContain('gx:LegalPerson')
      expect(subjects[1]['type']).toContain('gx:Issuer')
      expect(subjects[2]['type']).toContain('gx:VatID')
    })
    it('should retrieve credentialSubject from VerifiablePresentation with credential array with one credential', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(JSON.parse(vpArrayOfOneCredSingleSubject))
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(1)
    })
    it('should retrieve credentialSubject from VerifiablePresentation with credential being one credential', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(JSON.parse(vpCredentialObject))
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(1)
    })
    it('should retrieve credentialSubjects from VerifiablePresentation with one VC with multiple subjects', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(JSON.parse(vpCredentialObjectMultipleSubjects))
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(2)
    })
    it('should retrieve subjectType from VerifiablePresentation with one VC with single subject typed', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(JSON.parse(vpSubjectHasType))
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(1)
      expect(subjects[0]['type']).toContain('gx:LegalPerson')
    })
    it('should retrieve subjectType from VerifiablePresentation with one VC with single subject typed in array', () => {
      const subjects = service.extractCredentialSubjectsFromVerifiablePresentation(JSON.parse(vpSubjectHasTypeInArray))
      expect(subjects).toBeDefined()
      expect(subjects).toHaveLength(1)
      expect(subjects[0]['type']).toContain('gx:LegalPerson')
    })
  })
})

const shaclResult = `PREFIX dcat:    <http://www.w3.org/ns/dcat#>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX gx:      <https://w3id.org/gaia-x/development#>
PREFIX odrl:    <http://www.w3.org/ns/odrl/2/>
PREFIX rdf:     <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs:    <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema:  <https://schema.org/>
PREFIX sh:      <http://www.w3.org/ns/shacl#>
PREFIX xsd:     <http://www.w3.org/2001/XMLSchema#>

[ rdf:type     sh:ValidationReport;
  sh:conforms  false;
  sh:result    [ rdf:type                      sh:ValidationResult;
                 sh:focusNode                  <https://trusted-issuer.com/so.json#cs>;
                 sh:resultMessage              "Or at focusNode <https://trusted-issuer.com/data.json#cs>";
                 sh:resultPath                 gx:aggregationOfResources;
                 sh:resultSeverity             sh:Violation;
                 sh:sourceConstraintComponent  sh:OrConstraintComponent;
                 sh:sourceShape                [] ;
                 sh:value                      <https://trusted-issuer.com/data.json#cs>
               ];
  sh:result    [ rdf:type                      sh:ValidationResult;
                 sh:focusNode                  <https://trusted-issuer.com/so.json#cs>;
                 sh:resultMessage              "Closed[https://schema.org/description, https://w3id.org/gaia-x/development#dataAccountExport, https://w3id.org/gaia-x/development#dataProtectionRegime, https://w3id.org/gaia-x/development#serviceScope, https://w3id.org/gaia-x/development#providerContactInformation, https://w3id.org/gaia-x/development#legalDocuments, https://w3id.org/gaia-x/development#possiblePersonalDataTransfers, https://w3id.org/gaia-x/development#keyword, https://w3id.org/gaia-x/development#hostedOn, https://w3id.org/gaia-x/development#endpoint, https://w3id.org/gaia-x/development#provisionType, https://w3id.org/gaia-x/development#cryptographicSecurityStandards, https://w3id.org/gaia-x/development#aggregationOfResources, https://schema.org/name, https://w3id.org/gaia-x/development#dependsOn, https://w3id.org/gaia-x/development#subContractors, https://w3id.org/gaia-x/development#requiredMeasures, https://w3id.org/gaia-x/development#serviceOfferingTermsAndConditions, https://w3id.org/gaia-x/development#providedBy, https://w3id.org/gaia-x/development#customerInstructions, https://w3id.org/gaia-x/development#dataPortability, https://w3id.org/gaia-x/development#servicePolicy][http://www.w3.org/1999/02/22-rdf-syntax-ns#type] Property = <https://w3id.org/gaia-x/development#legallyBindingActs> : Object = _:Bc7b3a2a6b1647355895988ef482c5690";
                 sh:resultPath                 gx:legallyBindingActs;
                 sh:resultSeverity             sh:Violation;
                 sh:sourceConstraintComponent  sh:ClosedConstraintComponent;
                 sh:sourceShape                gx:ServiceOfferingShape;
                 sh:value                      [] 
               ];
  sh:result    [ rdf:type                      sh:ValidationResult;
                 sh:focusNode                  <https://trusted-issuer.com/so.json#cs>;
                 sh:resultMessage              "Closed[https://schema.org/description, https://w3id.org/gaia-x/development#dataAccountExport, https://w3id.org/gaia-x/development#dataProtectionRegime, https://w3id.org/gaia-x/development#serviceScope, https://w3id.org/gaia-x/development#providerContactInformation, https://w3id.org/gaia-x/development#legalDocuments, https://w3id.org/gaia-x/development#possiblePersonalDataTransfers, https://w3id.org/gaia-x/development#keyword, https://w3id.org/gaia-x/development#hostedOn, https://w3id.org/gaia-x/development#endpoint, https://w3id.org/gaia-x/development#provisionType, https://w3id.org/gaia-x/development#cryptographicSecurityStandards, https://w3id.org/gaia-x/development#aggregationOfResources, https://schema.org/name, https://w3id.org/gaia-x/development#dependsOn, https://w3id.org/gaia-x/development#subContractors, https://w3id.org/gaia-x/development#requiredMeasures, https://w3id.org/gaia-x/development#serviceOfferingTermsAndConditions, https://w3id.org/gaia-x/development#providedBy, https://w3id.org/gaia-x/development#customerInstructions, https://w3id.org/gaia-x/development#dataPortability, https://w3id.org/gaia-x/development#servicePolicy][http://www.w3.org/1999/02/22-rdf-syntax-ns#type] Property = <https://w3id.org/gaia-x/development#legallyBindingActs> : Object = _:B1e8fad3891b1c2ca33607f2a4bf06d18";
                 sh:resultPath                 gx:legallyBindingActs;
                 sh:resultSeverity             sh:Violation;
                 sh:sourceConstraintComponent  sh:ClosedConstraintComponent;
                 sh:sourceShape                gx:ServiceOfferingShape;
                 sh:value                      [] 
               ]
] .`

const vpArrayOfCredsSingleSubject = `{
  "@context": [
    "https://www.w3.org/ns/credentials/v2"
  ],
  "@type": [
    "VerifiablePresentation"
  ],
  "verifiableCredential": [
    {
      "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://w3id.org/gaia-x/development#"
      ],
      "id": "https://trusted-issuer.com/participant.json",
      "type": [
        "VerifiableCredential",
        "gx:LegalPerson"
      ],
      "issuer": "did:web:gaia-x.unicorn-home.com",
      "validFrom": "2024-08-05T13:31:52.852+00:00",
      "name": "Example Ltd LegalPerson",
      "credentialSubject": {
        "id": "https://trusted-issuer.com/participant.json#cs",
        "name": "Example Ltd",
        "headquartersAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        },
        "gx:registrationNumber": {
          "id": "https://trusted-issuer.com/lrn.json#cs"
        },
        "legalAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        }
      }
    },
    {
      "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://w3id.org/gaia-x/development#"
      ],
      "type": [
        "VerifiableCredential",
        "gx:Issuer"
      ],
      "validFrom": "2024-08-05T13:31:52.858+00:00",
      "id": "https://trusted-issuer.com/issuer.json",
      "issuer": "did:web:gaia-x.unicorn-home.com",
      "credentialSubject": {
        "id": "https://trusted-issuer.com/issuer.json#cs",
        "gaiaxTermsAndConditions": "4bd7554097444c960292b4726c2efa1373485e8a5565d94d41195214c5e0ceb3"
      }
    },
    {
      "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://w3id.org/gaia-x/development#"
      ],
      "type": [
        "VerifiableCredential",
        "gx:VatID"
      ],
      "id": "https://trusted-issuer.com/lrn.json",
      "issuer": "did:web:gaia-x.unicorn-home.com",
      "validFrom": "2024-08-05T13:31:52.859+00:00",
      "credentialSubject": {
        "id": "https://trusted-issuer.com/lrn.json#cs",
        "gx:vatID": "BE0762747721",
        "countryCode": "BE"
      },
      "evidence": [
        {
          "gx:evidenceURL": "http://ec.europa.eu/taxation_customs/vies/services/checkVatService",
          "gx:executionDate": "2024-05-15T12:10:23.900Z",
          "gx:evidenceOf": "gx:vatID"
        }
      ]
    }
  ]
}`
const vpArrayOfOneCredSingleSubject = `{
  "@context": [
    "https://www.w3.org/ns/credentials/v2"
  ],
  "@type": [
    "VerifiablePresentation"
  ],
  "verifiableCredential": [
    {
      "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://w3id.org/gaia-x/development#"
      ],
      "id": "https://trusted-issuer.com/participant.json",
      "type": [
        "VerifiableCredential",
        "gx:LegalPerson"
      ],
      "issuer": "did:web:gaia-x.unicorn-home.com",
      "validFrom": "2024-08-05T13:31:52.852+00:00",
      "name": "Example Ltd LegalPerson",
      "credentialSubject": {
        "id": "https://trusted-issuer.com/participant.json#cs",
        "name": "Example Ltd",
        "headquartersAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        },
        "gx:registrationNumber": {
          "id": "https://trusted-issuer.com/lrn.json#cs"
        },
        "legalAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        }
      }
    }
  ]
}`
const vpCredentialObject = `{
  "@context": [
    "https://www.w3.org/ns/credentials/v2"
  ],
  "@type": [
    "VerifiablePresentation"
  ],
  "verifiableCredential": 
    {
      "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://w3id.org/gaia-x/development#"
      ],
      "id": "https://trusted-issuer.com/participant.json",
      "type": [
        "VerifiableCredential",
        "gx:LegalPerson"
      ],
      "issuer": "did:web:gaia-x.unicorn-home.com",
      "validFrom": "2024-08-05T13:31:52.852+00:00",
      "name": "Example Ltd LegalPerson",
      "credentialSubject": {
        "id": "https://trusted-issuer.com/participant.json#cs",
        "name": "Example Ltd",
        "headquartersAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        },
        "gx:registrationNumber": {
          "id": "https://trusted-issuer.com/lrn.json#cs"
        },
        "legalAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        }
      }
    }
}`

const vpCredentialObjectMultipleSubjects = `{
  "@context": [
    "https://www.w3.org/ns/credentials/v2"
  ],
  "@type": [
    "VerifiablePresentation"
  ],
  "verifiableCredential": 
    {
      "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://w3id.org/gaia-x/development#"
      ],
      "id": "https://trusted-issuer.com/participant.json",
      "type": [
        "VerifiableCredential"
      ],
      "issuer": "did:web:gaia-x.unicorn-home.com",
      "validFrom": "2024-08-05T13:31:52.852+00:00",
      "name": "Example Ltd LegalPerson",
      "credentialSubject": [{
        "id": "https://trusted-issuer.com/participant.json#cs",
        "@type":"gx:LegalPerson",
        "name": "Example Ltd",
        "headquartersAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        },
        "gx:registrationNumber": {
          "id": "https://trusted-issuer.com/lrn.json#cs"
        },
        "legalAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        }
      },{
        "id": "https://trusted-issuer.com/issuer.json#cs",
        "@type":"gx:Issuer",
        "gaiaxTermsAndConditions": "4bd7554097444c960292b4726c2efa1373485e8a5565d94d41195214c5e0ceb3"
      }]
    }
}`
const vpSubjectHasType = `{
  "@context": [
    "https://www.w3.org/ns/credentials/v2"
  ],
  "@type": [
    "VerifiablePresentation"
  ],
  "verifiableCredential": 
    {
      "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://w3id.org/gaia-x/development#"
      ],
      "id": "https://trusted-issuer.com/participant.json",
      "type": [
        "VerifiableCredential"
      ],
      "issuer": "did:web:gaia-x.unicorn-home.com",
      "validFrom": "2024-08-05T13:31:52.852+00:00",
      "name": "Example Ltd LegalPerson",
      "credentialSubject": [{
        "id": "https://trusted-issuer.com/participant.json#cs",
        "type":"gx:LegalPerson",
        "name": "Example Ltd",
        "headquartersAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        },
        "gx:registrationNumber": {
          "id": "https://trusted-issuer.com/lrn.json#cs"
        },
        "legalAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        }
      }]
    }
}`
const vpSubjectHasTypeInArray = `{
  "@context": [
    "https://www.w3.org/ns/credentials/v2"
  ],
  "@type": [
    "VerifiablePresentation"
  ],
  "verifiableCredential": 
    {
      "@context": [
        "https://www.w3.org/ns/credentials/v2",
        "https://w3id.org/gaia-x/development#"
      ],
      "id": "https://trusted-issuer.com/participant.json",
      "type": [
        "VerifiableCredential"
      ],
      "issuer": "did:web:gaia-x.unicorn-home.com",
      "validFrom": "2024-08-05T13:31:52.852+00:00",
      "name": "Example Ltd LegalPerson",
      "credentialSubject": [{
        "id": "https://trusted-issuer.com/participant.json#cs",
        "type":["gx:LegalPerson"],
        "name": "Example Ltd",
        "headquartersAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        },
        "gx:registrationNumber": {
          "id": "https://trusted-issuer.com/lrn.json#cs"
        },
        "legalAddress": {
          "type": "gx:Address",
          "countryCode": "FR",
          "countryName": "France"
        }
      }]
    }
}`
