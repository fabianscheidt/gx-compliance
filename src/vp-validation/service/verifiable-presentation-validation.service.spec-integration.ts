import { Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'

import { DateTime } from 'luxon'
import { Driver, Session } from 'neo4j-driver'

import { VerifiablePresentation } from '../../common/constants'
import { ValidationResult } from '../../common/dto'
import { ConformityLevelEnum } from '../../common/enum/conformity-level.enum'
import { ConfigServiceMock } from '../../tests/config.service.mock'
import { memgraphTestSuite } from '../../tests/memgraph-test-suite'
import { VpContentValidationModule } from '../vp-content-validation.module'
import validVerifiablePresentation from './fixtures/valid-verifiable-presentation.json'
import { VerifiablePresentationValidationService } from './verifiable-presentation-validation.service'

describe('VerifiablePresentationValidationService', () => {
  memgraphTestSuite((driver: Promise<Driver>, url: Promise<string>) => {
    let service: VerifiablePresentationValidationService

    beforeAll(async () => {
      process.env.dburl = await url

      const moduleFixture = await Test.createTestingModule({
        imports: [VpContentValidationModule]
      })
        .overrideProvider('privateKey')
        .useValue(null)
        .overrideProvider(ConfigService)
        .useValue(new ConfigServiceMock({}))
        .setLogger(new Logger())
        .compile()

      service = moduleFixture.get(VerifiablePresentationValidationService)
    })

    // TODO uncomment this when the ontology is up-to-date
    xit('should validate a verifiable presentation', async () => {
      const validationResult: ValidationResult = await service.validate(validVerifiablePresentation, ConformityLevelEnum.BASIC_CONFORMITY)

      expect(validationResult.conforms).toBe(true)
      expect(validationResult.results).toHaveLength(0)

      const session: Session = (await driver).session()
      try {
        const result = await session.executeRead(tx => tx.run('MATCH (n) RETURN COUNT(*) AS nodeCount'))
        expect(result.records[0].get('nodeCount').toNumber()).toEqual(0)
      } finally {
        await session.close()
      }
    })

    // TODO uncomment this when the ontology is up-to-date
    xit('should throw an error when verifiable presentation is invalid', async () => {
      const invalidVerifiablePresentation: VerifiablePresentation = structuredClone(validVerifiablePresentation)
      const expiredValidUntil: string = DateTime.now().minus({ day: 10 }).toISO()

      invalidVerifiablePresentation.verifiableCredential[0].validUntil = expiredValidUntil
      invalidVerifiablePresentation.verifiableCredential.find(vc => vc.type.includes('gx:Issuer')).credentialSubject.gaiaxTermsAndConditions =
        'invalidTs&Cs'

      const validationResult: ValidationResult = await service.validate(invalidVerifiablePresentation, ConformityLevelEnum.BASIC_CONFORMITY)

      expect(validationResult.conforms).toBe(false)
      expect(validationResult.results).toEqual([
        'trusted-issuer.com has an incorrect Gaia-X terms and conditions hash',
        `VC https://trusted-issuer.com/participant.json validUntil ${expiredValidUntil} is in the past`
      ])

      const session: Session = (await driver).session()
      try {
        const result = await session.executeRead(tx => tx.run('MATCH (n) RETURN COUNT(*) AS nodeCount'))
        expect(result.records[0].get('nodeCount').toNumber()).toEqual(0)
      } finally {
        await session.close()
      }
    })
  })
})
