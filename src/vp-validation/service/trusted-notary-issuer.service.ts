import { HttpService } from '@nestjs/axios'
import { Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { Cron, CronExpression } from '@nestjs/schedule'

import { firstValueFrom } from 'rxjs'

@Injectable()
export class TrustedNotaryIssuerService {
  private readonly logger: Logger = new Logger(TrustedNotaryIssuerService.name)
  private readonly registryUrl: string
  private readonly developmentEnvironment: boolean

  private trustedNotaryIssuersCache?: Array<string> = null

  constructor(private readonly configService: ConfigService, private readonly httpService: HttpService) {
    this.registryUrl = this.configService.get('REGISTRY_URL', 'https://registry.gaia-x.eu/development')
    this.developmentEnvironment = this.configService.get('production', 'false') === 'false'
  }

  @Cron(CronExpression.EVERY_10_MINUTES)
  clearTrustedNotaryIssuersCache() {
    this.logger.log('Clearing up trustedNotaryIssuers cache')
    this.trustedNotaryIssuersCache = null
  }

  async isTrusted(issuer: string): Promise<boolean> {
    return this.developmentEnvironment || (await this.retrieveTrustedNotaryIssuers()).some(trustedIssuer => trustedIssuer === issuer)
  }

  async retrieveTrustedNotaryIssuers(): Promise<string[]> {
    if (this.trustedNotaryIssuersCache === null) {
      this.trustedNotaryIssuersCache = (
        await firstValueFrom(this.httpService.get<Array<string>>(`${this.registryUrl}/api/trusted-issuers/registration-notary`))
      ).data
    }

    return this.trustedNotaryIssuersCache
  }
}
