import crypto from 'crypto'
import { SignJWT, exportJWK } from 'jose'
import { pki } from 'node-forge'

import { CertificateBuilderUtilsSpec } from './certificate-builder-utils.spec'
import * as vc from './fixtures/v2/vc.json'
import * as vp from './fixtures/v2/vp.json'

export class VCJwtUtilsSpec {
  static async preparePayloadsAndKeys() {
    const { keypair } = CertificateBuilderUtilsSpec.createCertificateAndKeyPair(12)
    const privateKey = crypto.createPrivateKey(pki.privateKeyToPem(keypair.privateKey))
    const jwtVC = await new SignJWT(vc)
      .setProtectedHeader({ alg: 'PS256', iss: 'did:web:example.org', kid: 'did:web:example.org#key' })
      .sign(privateKey)
    let vpJSON = JSON.stringify(vp)
    vpJSON = vpJSON.replace('µµvcµµ', jwtVC)
    const jwtVP = await new SignJWT(JSON.parse(vpJSON))
      .setProtectedHeader({ alg: 'PS256', iss: 'did:web:example.org', kid: 'did:web:example.org#key' })
      .sign(privateKey)

    const jwtVCWithoutIss = await new SignJWT(vc)
      .setProtectedHeader({
        alg: 'PS256',
        kid: 'did:web:example.org#key'
      })
      .sign(privateKey)
    const jwtVCWithoutKid = await new SignJWT(vc)
      .setProtectedHeader({
        alg: 'PS256',
        iss: 'did:web:example.org'
      })
      .sign(privateKey)
    const jwk = await exportJWK(crypto.createPublicKey(pki.publicKeyToPem(keypair.publicKey)))
    jwk.alg = 'PS256'
    return { jwtVC, jwtVP, jwtVCWithoutIss, jwtVCWithoutKid, jwk }
  }
}

describe('JwtVcTestUtils', () => {
  it('is used only to generate jwt-vc', () => {
    // Nothing
  })
})
