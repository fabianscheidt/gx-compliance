import { OfflineDocumentLoaderBuilder } from '@gaia-x/json-web-signature-2020'
import jsonld from 'jsonld'
import neo4j, { Driver, Session } from 'neo4j-driver'
import { GenericContainer, StartedTestContainer } from 'testcontainers'

import { RdfGraphUtils } from '../common/utils/rdf-graph.utils'
import CredentialsV2 from '../contexts/credentials_v2_context.json'
import SchemaContext from '../contexts/schema_context.json'

jsonld.documentLoader = new OfflineDocumentLoaderBuilder()
  .addContext('https://schema.org/', SchemaContext)
  .addContext('http://schema.org/', SchemaContext)
  .addContext('https://www.w3.org/ns/credentials/v2', CredentialsV2)
  .addCachedContext('https://w3id.org/gaia-x/development#')
  .build()

export const memgraphTestSuite = (testCases: (driver: Promise<Driver>, url: Promise<string>) => void) => {
  jest.setTimeout(60000)

  let memgraphContainer: StartedTestContainer
  let url: string
  let driver: Driver

  let urlResolve: (string) => void
  const urlPromise: Promise<string> = new Promise(resolve => {
    urlResolve = resolve
  })

  let driverResolve: (Driver) => void
  const driverPromise: Promise<Driver> = new Promise(resolve => {
    driverResolve = resolve
  })

  beforeAll(async () => {
    // Can be used with the image: memgraph/memgraph-platform:latest for integrated web client exposed on 3000
    memgraphContainer = await new GenericContainer('memgraph/memgraph:latest').withExposedPorts(7687).start()

    url = `bolt://${memgraphContainer.getHost()}:${memgraphContainer.getMappedPort(7687)}`
    urlResolve(url)

    driver = neo4j.driver(url)
    driverResolve(driver)
  })

  afterAll(async () => {
    await driver.close()
    await memgraphContainer.stop()
  })

  afterEach(async () => {
    const session: Session = driver.session()
    try {
      await session.run('MATCH (n) DETACH DELETE n')
      console.log('MemGraph database cleared successfully')
    } catch (error) {
      console.error(error)
    } finally {
      await session.close()
    }
  })

  testCases(driverPromise, urlPromise)
}

export async function insertObjectInMemGraph(driver: Driver, vpUuid: string, object: any) {
  const quads = await jsonld.toRDF(object, { format: 'application/n-quads' })
  const queries: string[] = RdfGraphUtils.quadsToQueries(vpUuid, quads)

  const session = driver.session()
  for (const query of queries) {
    await session.executeWrite(tx => tx.run(query))
  }

  await session.close()
}
