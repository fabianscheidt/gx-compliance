import { HttpModule } from '@nestjs/axios'
import { Global, Module, OnApplicationBootstrap } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'

import { PrometheusModule } from '@willsoto/nestjs-prometheus'
import { KeyLike } from 'jose'

import { CertificateExpirationBatch } from '../batch/certificate-expiration.batch'
import { RegistryService } from '../vp-validation/service/registry.service'
import { ShaclService } from '../vp-validation/service/shacl.service'
import { DidResolverProvider } from './providers/did-resolver.provider'
import { DocumentLoaderProvider } from './providers/document-loader.provider'
import { PrivateKeyAlgorithmProvider } from './providers/private-key-algorithm.provider'
import { PrivateKeyProvider } from './providers/private-key.provider'
import { DIDService } from './services/did.service'
import { EvidenceService } from './services/evidence.service'
import { ExpirationDateService } from './services/expiration-date.service'
import { TimeService } from './services/time.service'
import { CertificateUtil, createDidDocument } from './utils'

const prometheusModule = PrometheusModule.register({
  customMetricPrefix: 'gaia_x'
})
@Global()
@Module({
  imports: [ConfigModule.forRoot(), HttpModule, prometheusModule],
  providers: [
    CertificateExpirationBatch,
    DidResolverProvider.create(),
    DIDService,
    DocumentLoaderProvider.create(),
    ExpirationDateService,
    EvidenceService,
    RegistryService,
    PrivateKeyAlgorithmProvider.create(),
    PrivateKeyProvider.create(),
    ShaclService,
    TimeService
  ],
  exports: [
    ConfigModule,
    HttpModule,
    DIDService,
    EvidenceService,
    RegistryService,
    ShaclService,
    'privateKey',
    'privateKeyAlgorithm',
    'documentLoader',
    prometheusModule
  ]
})
export class CommonModule implements OnApplicationBootstrap {
  constructor(private readonly configService: ConfigService) {}

  async onApplicationBootstrap() {
    const x509Certificate: string = this.configService.get<string>('X509_CERTIFICATE')
    const privateKeyAlg: string = this.configService.get<string>('PRIVATE_KEY_ALG', 'PS256')

    const certificate: KeyLike = await CertificateUtil.loadCertificate(x509Certificate)
    await createDidDocument(certificate, privateKeyAlg)
  }
}
