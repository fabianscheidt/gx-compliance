import { HttpModule } from '@nestjs/axios'
import { ConflictException } from '@nestjs/common'
import { Test } from '@nestjs/testing'

import { DidResolver } from '@gaia-x/json-web-signature-2020'
import crypto from 'crypto'
import { pki } from 'node-forge'

import { CertificateBuilderUtilsSpec } from '../../tests/certificate-builder-utils.spec'
import { CommonModule } from '../common.module'
import { DidResolverProvider } from '../providers/did-resolver.provider'
import { DIDService } from './did.service'

describe('DIDService', () => {
  let didService: DIDService
  const didResolverMock = new DidResolver()
  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [CommonModule, HttpModule],
      providers: [DidResolverProvider.create(), DIDService]
    })
      .overrideProvider(DidResolver)
      .useValue(didResolverMock)
      .compile()
    didService = moduleFixture.get(DIDService)
  })
  it('should throw an exception when the did is not resolvable', async () => {
    // Given the did is not resolvable
    jest.spyOn(didResolverMock, 'resolve').mockImplementation(() => {
      throw new Error('Unable to resolve domain')
    })
    // When getting the DID from the service
    // Then I get a Conflict Exception
    try {
      await didService.getDIDDocumentFromDID('did:web:toto.eu')
      fail()
    } catch (error) {
      expect(error).toBeDefined()
      expect(error).toBeInstanceOf(ConflictException)
      expect(error.message).toMatch(/Unable to retrieve your did .+ Unable to resolve domain/)
    }
  })
  it('should throw an exception when the did verificationMethod is absent', async () => {
    //Given the DID does not contain a field verificationMethod
    jest.spyOn(didResolverMock, 'resolve').mockImplementation(() => {
      return new Promise(resolve => {
        resolve({
          '@context': ['https://www.w3.org/ns/did/v1'],
          id: 'did:web:example.org'
        })
      })
    })
    //When I'm getting the DID
    //Then I get a Conflict Exception
    try {
      await didService.getDIDDocumentFromDID('did:web:toto.eu')
      fail()
    } catch (error) {
      expect(error).toBeDefined()
      expect(error).toBeInstanceOf(ConflictException)
      expect(error.message).toMatch(/DID .* does not contain the verificationMethod array/)
    }
  })
  it('should retrieve DID using did-resolver on first call', async () => {
    //Given the DID does not contain a field verificationMethod
    jest.spyOn(didResolverMock, 'resolve').mockImplementation(() => {
      return new Promise(resolve => {
        resolve({
          '@context': ['https://www.w3.org/ns/did/v1'],
          id: 'did:web:example.org',
          verificationMethod: [
            {
              id: 'did:web:example.org#key',
              type: 'JsonWebKey2020',
              controller: 'did:web:example.org'
            }
          ]
        })
      })
    })
    //When I'm getting the DID
    //Then I get a Conflict Exception
    try {
      await didService.getDIDDocumentFromDID('did:web:toto.eu')
      expect(didResolverMock.resolve).toHaveBeenNthCalledWith(1, 'did:web:toto.eu')
    } catch (error) {
      fail()
    }
  })
  it('should retrieve DID using cache on after first call', async () => {
    //Given the DID does not contain a field verificationMethod
    jest.spyOn(didResolverMock, 'resolve').mockImplementation(() => {
      return new Promise(resolve => {
        resolve({
          '@context': ['https://www.w3.org/ns/did/v1'],
          id: 'did:web:example.org',
          verificationMethod: [
            {
              id: 'did:web:example.org#key',
              type: 'JsonWebKey2020',
              controller: 'did:web:example.org'
            }
          ]
        })
      })
    })
    //When I'm getting the DID
    //Then I get a Conflict Exception
    try {
      await didService.getDIDDocumentFromDID('did:web:toto.eu')
      await didService.getDIDDocumentFromDID('did:web:toto.eu')
      expect(didResolverMock.resolve).toHaveBeenNthCalledWith(1, 'did:web:toto.eu')
    } catch (error) {
      fail()
    }
  })
  it('should be able to compare a public key and the associated x5c', async () => {
    const { cert, keypair } = CertificateBuilderUtilsSpec.createCertificateAndKeyPair(1)
    const x5c = [pki.certificateToPem(cert).replace(/[\r\n|\r|\n]/g, '')]
    expect((await didService.checkx5cMatchesPublicKey(crypto.createPublicKey(pki.publicKeyToPem(keypair.publicKey)), x5c)).conforms).toBeTruthy()
  })
  it('should reject a public key and a wrong x5c', async () => {
    const { keypair } = CertificateBuilderUtilsSpec.createCertificateAndKeyPair(1)
    const { cert } = CertificateBuilderUtilsSpec.createCertificateAndKeyPair(1)
    const x5c = [pki.certificateToPem(cert).replace(/[\r\n|\r|\n]/g, '')]
    expect((await didService.checkx5cMatchesPublicKey(crypto.createPublicKey(pki.publicKeyToPem(keypair.publicKey)), x5c)).conforms).toBeFalsy()
  })
  it('should throw an exception if publicKey or certificate are not valid for x5c validation', async () => {
    const { keypair } = CertificateBuilderUtilsSpec.createCertificateAndKeyPair(1)
    const x5c = ['toto', 'tata']
    await expect(async () => {
      await didService.checkx5cMatchesPublicKey(crypto.createPublicKey(pki.publicKeyToPem(keypair.publicKey)), x5c)
    }).rejects.toThrow(/Could not confirm X509 public key with certificate chain.*/)
  })
})
