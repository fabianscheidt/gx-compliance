import { VerifiableCredentialDto, VerifiablePresentationDto } from '../dto'

export const JWT_VC_CONTENT_TYPES = ['application/vc+jwt', 'application/jwt']
export const JWT_VP_CONTENT_TYPE = 'application/vp+ld+jwt'
export const DEFAULT_VC_LIFE_EXPECTANCY_IN_DAYS = '90'
export const RULES_VERSION = 'PRLD-24.04_pre'
export const CTY_VALUE = 'vc'
export const TYP_VALUE = 'vc-ld+jwt'
export const NORMALIZATION_ALG = 'RFC8785:JCS'

export type VerifiablePresentation = VerifiablePresentationDto<VerifiableCredentialDto<any>>

export const EEA_COUNTRY_NAME_ALPHA2 = [
  'AT',
  'BE',
  'BG',
  'CY',
  'CZ',
  'HR',
  'DE',
  'DK',
  'ES',
  'EE',
  'FI',
  'FR',
  'GR',
  'HU',
  'IE',
  'IS',
  'IT',
  'LV',
  'LI',
  'LT',
  'LU',
  'MT',
  'NL',
  'NO',
  'PL',
  'PT',
  'RO',
  'SK',
  'SI',
  'SE'
]

/**
 * List of adequately protected third countries according to GDPR's Chapter V.
 *
 * The list has been extracted from <a href="https://commission.europa.eu/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en">https://commission.europa.eu/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en</a>
 * @private
 */
export const PROTECTED_THIRD_COUNTRY_CODES: string[] = [
  'AD', // Andorra
  'AR', // Argentina
  'CA', // Canada
  'FO', // Faroe Islands
  'GG', // Guernsey
  'IL', // Israel
  'IM', // Isle of Man
  'JP', // Japan
  'JE', // Jersey
  'NZ', // New Zealand
  'KR', // Republic of Korea
  'CH', // Switzerland
  'GB', // United Kingdom
  'US', // United States
  'UY' // Uruguay
]
