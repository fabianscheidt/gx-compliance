import { CredentialSubjectDto, VerifiableCredentialDto } from '../dto'
import { CompliantCredentialSubjectMapper } from './compliant-credential-subject-mapper'

export class ComplianceCredentialMapper extends CompliantCredentialSubjectMapper {
  type(): string {
    return 'gx:ComplianceEvidence'
  }

  public map(verifiableCredential: VerifiableCredentialDto<CredentialSubjectDto>) {
    return super.map(verifiableCredential)
  }
}
