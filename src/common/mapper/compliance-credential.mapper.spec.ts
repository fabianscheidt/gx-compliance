import canonicalize from 'canonicalize'

import * as packageJSON from '../../../package.json'
import { NORMALIZATION_ALG, RULES_VERSION } from '../constants'
import { CredentialSubjectDto, VerifiableCredentialDto } from '../dto'
import { ComplianceEvidenceDTO } from '../dto/compliant-credential-subject.dto'
import { HashingUtils } from '../utils/hashing.utils'
import { ComplianceCredentialMapper } from './compliance-credential.mapper'

describe('ComplianceCredentialMapper', () => {
  let complianceCredentialMapper: ComplianceCredentialMapper

  beforeEach(() => {
    complianceCredentialMapper = new ComplianceCredentialMapper()
  })

  it('should map a verifiable credential to a compliance credential', () => {
    const verifiableCredential: VerifiableCredentialDto<CredentialSubjectDto> = {
      id: 'did:web:gaia-x.eu#test-verifiable-credential',
      type: ['VerifiableCredential'],
      '@context': undefined,
      credentialSubject: {
        id: 'did:web:gaia-x.eu#test-credential-subject',
        type: 'gx:LegalParticipant'
      },
      validFrom: '',
      validUntil: '',
      issuer: ''
    }

    const result: ComplianceEvidenceDTO = complianceCredentialMapper.map(verifiableCredential)

    expect(result).toEqual({
      id: verifiableCredential.id ?? verifiableCredential['@id'],
      type: 'gx:ComplianceEvidence',
      'gx:rulesVersion': RULES_VERSION,
      'gx:engineVersion': packageJSON.version,
      'gx:integrity': `sha256-${HashingUtils.sha256(canonicalize(verifiableCredential))}`,
      'gx:integrityNormalization': NORMALIZATION_ALG,
      'gx:originalType': 'gx:LegalParticipant'
    })
  })

  it('should map a verifiable credential with multiple credential subject types to a compliance credential', () => {
    const verifiableCredential: VerifiableCredentialDto<CredentialSubjectDto> = {
      id: 'did:web:gaia-x.eu#test-verifiable-credential',
      type: ['VerifiableCredential'],
      '@context': undefined,
      credentialSubject: {
        id: 'did:web:gaia-x.eu#test-credential-subject',
        type: ['gx:LegalParticipant', 'gx:AnotherLegalParticipant']
      },
      validFrom: '',
      validUntil: '',
      issuer: ''
    }

    const result: ComplianceEvidenceDTO = complianceCredentialMapper.map(verifiableCredential)

    expect(result).toEqual({
      id: verifiableCredential.id ?? verifiableCredential['@id'],
      type: 'gx:ComplianceEvidence',
      'gx:integrity': `sha256-${HashingUtils.sha256(canonicalize(verifiableCredential))}`,
      'gx:integrityNormalization': NORMALIZATION_ALG,
      'gx:rulesVersion': RULES_VERSION,
      'gx:engineVersion': packageJSON.version,
      'gx:originalType': 'gx:LegalParticipant,gx:AnotherLegalParticipant'
    })
  })
})
