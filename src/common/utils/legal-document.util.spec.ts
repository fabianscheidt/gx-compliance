import { LegalDocument } from '../model/legal-document'
import { LegalDocumentProperty, LegalDocumentUtil } from './legal-document.util'

describe('LegalDocumentUtil', () => {
  it('should map legal document properties to a legal document', () => {
    const properties: LegalDocumentProperty[] = [
      {
        type: '_https_w3id_org_gaia_x_development_url_',
        value: 'https://example.org/my-url'
      },
      {
        type: '_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_',
        value: '<https://w3id.org/gaia-x/development#LegallyBindingAct>'
      },
      {
        type: '_https_w3id_org_gaia_x_development_mimeTypes_',
        value: 'application/pdf'
      },
      {
        type: '_https_w3id_org_gaia_x_development_mimeTypes_',
        value: 'application/json'
      },
      {
        type: '_https_w3id_org_gaia_x_development_governingLawCountries_',
        value: 'BE'
      },
      {
        type: '_https_w3id_org_gaia_x_development_governingLawCountries_',
        value: 'FR'
      },
      {
        type: '_https_w3id_org_gaia_x_development_involvedParties_',
        value: 'https://example.org/legal-person-1'
      },
      {
        type: '_https_w3id_org_gaia_x_development_involvedParties_',
        value: 'https://example.org/legal-person-2'
      }
    ]

    const legalDocument: LegalDocument = LegalDocumentUtil.mapFromProperties(properties)

    expect(legalDocument).toEqual({
      url: 'https://example.org/my-url',
      type: 'w3id.org/gaia-x/development#LegallyBindingAct',
      mimeTypes: ['application/pdf', 'application/json'],
      governingLawCountries: ['BE', 'FR'],
      involvedParties: ['https://example.org/legal-person-1', 'https://example.org/legal-person-2']
    })
  })

  it.each(['https://gaia-x.eu/legal-document.pdf', 'ftp://gaia-x.eu/legal-document.pdf', 'email://gaia-x@test.com'])(
    'should validate valid legal document',
    (url: string) => {
      LegalDocumentUtil.isValid({
        url,
        type: 'w3id.org/gaia-x/development#LegallyBindingAct',
        mimeTypes: ['application/pdf'],
        governingLawCountries: ['FR'],
        involvedParties: []
      })
    }
  )

  it.each([null, '', ' ', 'not-a-uri'])('should throw an error if url is invalid', (url: string) => {
    expect(() =>
      LegalDocumentUtil.isValid({
        url,
        type: 'w3id.org/gaia-x/development#LegallyBindingAct',
        mimeTypes: ['application/pdf'],
        governingLawCountries: ['DE'],
        involvedParties: []
      })
    ).toThrowError(`Legal document url '${url}' is invalid`)
  })
})
