export class RdfGraphUtils {
  private static readonly NQUAD_REGEX = /(_:\S+|<[^<>]+>|"([^"]*)"(?:\^\^\S+)?)/g

  static quadsToQueries(vpUUID: string, quads: string): string[] {
    const edges = []
    const nodes = []
    const rdfEntries = quads
      .split('\n')
      .filter(quad => quad)
      .map(quad => this.quadToRDFEntry(quad))
      .sort(this.compareRDFEntryFn)
    rdfEntries.forEach(rdfEntry => {
      this.insertInNodesIfNotExisting(nodes, rdfEntry.subject)
      // In case we have a node with the ID of this literal, we point to the existing node
      this.renameObjectIfMatchingAnExistingNode(nodes, rdfEntry)
      this.insertInNodesIfNotExisting(nodes, rdfEntry.object)

      edges.push({ from: rdfEntry.subject, to: rdfEntry.object, predicate: rdfEntry.predicate })
    })
    const queries = []
    queries.push(
      ...nodes.map(node => {
        const nodeName = this.prepareNodeNameForGraph(vpUUID + node)
        return `CREATE (${nodeName}:${this.prepareNodeNameForGraph(node)} {id:'${nodeName}', value:'${node}', vpID:'${vpUUID}'});\n`
      })
    )
    queries.push(
      ...edges.map(edge => {
        return `MATCH (a),(b)
        WHERE a.id='${this.prepareNodeNameForGraph(vpUUID + edge.from)}' AND b.id='${this.prepareNodeNameForGraph(vpUUID + edge.to)}'
        CREATE (a)-[r:${this.prepareEdgeNameForGraph(edge.predicate)} {pType:"${this.prepareEdgeNameForGraph(edge.predicate)}"}]->(b);`
      })
    )
    return queries
  }

  static quadToRDFEntry(quad: string) {
    const matches = []
    for (const match of quad.matchAll(this.NQUAD_REGEX)) {
      // match[2] contains string entries without double quotes (omitting there IRI type ref)
      // match[1] is used for all other matches
      matches.push(match[2] ?? match[1])
    }

    return {
      subject: matches[0],
      predicate: matches[1],
      object: matches[2],
      graph: matches[3]
    }
  }

  static compareRDFEntryFn = (a, b) => {
    if (a.object?.startsWith('<') && !b.object?.startsWith('<')) {
      return -1
    } else if (b.object?.startsWith('<')) {
      return 1
    }
    return 0
  }

  static prepareNodeNameForGraph(name: string): string {
    if (!name || name === '') {
      return ''
    } else if ('.' === name) {
      return 'root'
    } else if (name === '""') {
      return '_empty_'
    }
    return this.sanitizeNames(name)
  }

  static prepareEdgeNameForGraph(name: string): string {
    if (!name) {
      return 'childOf'
    }
    return this.sanitizeNames(name)
  }

  static sanitizeNames(name: string) {
    let sanitizedName = name.replace(/[\W_]+/g, '_')
    if (!sanitizedName.startsWith('_')) {
      sanitizedName = '_' + sanitizedName
    }

    return sanitizedName
  }

  static insertInNodesIfNotExisting(nodes: any[], nodeName) {
    if (nodes.indexOf(nodeName) < 0 && !!nodeName) {
      nodes.push(nodeName)
    }
  }

  /**
   * This allows JSON-LD to use string URI and not only {"@id"} structure
   * @param nodes the existing node list
   * @param rdfEntry an RDF entry composed of subject predicate object
   */
  static renameObjectIfMatchingAnExistingNode = (nodes: any[], rdfEntry) => {
    if (!rdfEntry || !rdfEntry.object) {
      return
    }
    const renamedEntry = rdfEntry.object.replace(/"(.*)"/g, '<$1>')
    if (rdfEntry.object.startsWith('"') && nodes.indexOf(renamedEntry) > -1) {
      rdfEntry.object = renamedEntry
    }
  }
}
