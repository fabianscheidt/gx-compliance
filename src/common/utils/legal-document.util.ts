import { LegalDocument } from '../model/legal-document'
import { graphValueFormat } from './graph-value-format'
import { UrlUtils } from './url.utils'

export interface LegalDocumentProperty {
  type: string
  value: string
}

export class LegalDocumentUtil {
  static mapFromProperties(properties: LegalDocumentProperty[]): LegalDocument {
    const legalDocument: LegalDocument = { type: '', mimeTypes: [], url: '', involvedParties: [], governingLawCountries: [] }

    for (const property of properties) {
      if (property.type === '_https_w3id_org_gaia_x_development_url_') {
        legalDocument.url = property.value
      }

      if (property.type === '_http_www_w3_org_1999_02_22_rdf_syntax_ns_type_') {
        legalDocument.type = graphValueFormat(property.value)
      }

      if (property.type === '_https_w3id_org_gaia_x_development_mimeTypes_') {
        legalDocument.mimeTypes.push(property.value)
      }

      if (property.type === '_https_w3id_org_gaia_x_development_governingLawCountries_') {
        legalDocument.governingLawCountries.push(property.value)
      }

      if (property.type === '_https_w3id_org_gaia_x_development_involvedParties_') {
        legalDocument.involvedParties.push(property.value)
      }
    }

    return legalDocument
  }

  static isValid(legalDocument: LegalDocument): boolean | void {
    if (!legalDocument.url || !legalDocument.url.trim()) {
      throw new Error(`Legal document url '${legalDocument.url}' is invalid`)
    }

    if (!UrlUtils.isValid(legalDocument.url)) {
      throw new Error(`Legal document url '${legalDocument.url}' is invalid`)
    }

    return true
  }
}
