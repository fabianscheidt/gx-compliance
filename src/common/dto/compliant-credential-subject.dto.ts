/**
 * DTO class representing a compliance response credential subject signed by Gaia-X
 */
export class CompliantCredentialSubjectDto {
  id: string
  'gx:evidence': ComplianceEvidenceDTO[]
}

export class ComplianceEvidenceDTO {
  id: string
  type: string
  'gx:integrity': string
  'gx:integrityNormalization': string
  'gx:engineVersion': string
  'gx:rulesVersion': string
  'gx:originalType': string | string[]
}
