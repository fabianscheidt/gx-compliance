import { FactoryProvider } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { DocumentLoader, OfflineDocumentLoaderBuilder } from '@gaia-x/json-web-signature-2020'
import jsonld from 'jsonld'

import CredentialsV2 from '../../contexts/credentials_v2_context.json'
import SchemaContext from '../../contexts/schema_context.json'

export class DocumentLoaderProvider {
  static create(): FactoryProvider<DocumentLoader> {
    return {
      provide: 'documentLoader',
      inject: [ConfigService],
      useFactory: (configService: ConfigService): DocumentLoader => {
        if (configService.get<string>('USE_OFFLINE_DOCUMENT_LOADER') === 'true') {
          return new OfflineDocumentLoaderBuilder()
            .addContext('https://schema.org/', SchemaContext)
            .addContext('http://schema.org/', SchemaContext)
            .addContext('https://www.w3.org/ns/credentials/v2', CredentialsV2)
            .addCachedContext('https://w3id.org/gaia-x/development#')
            .build()
        }

        return jsonld.documentLoader
      }
    }
  }
}
