export enum ConformityLevelEnum {
  BASIC_CONFORMITY,
  LABEL_LEVEL_1,
  LABEL_LEVEL_2,
  LABEL_LEVEL_3
}
