import { BadRequestException, Injectable, InternalServerErrorException } from '@nestjs/common'

import { DIDDocument } from 'did-resolver'
import { ProtectedHeaderParameters, decodeProtectedHeader, jwtVerify } from 'jose'

import { DIDService } from '../../common/services/did.service'

@Injectable()
export class JwtSignatureValidationService {
  constructor(private readonly didService: DIDService) {}

  /**
   * Takes a JWT-VP and extracts all the VCs in it.
   * @param jwt a JWT-VP
   * @throws BadRequestException if the string is not a JWT, if the headers iss & kid are missing, if the signature is invalid
   * @throws ConflictException if the JWT does not contain a VerifiablePresentation containing VerifiableCredential
   */
  async validateJWTAndConvertToVCs(jwt: string) {
    const decodedJWT: ProtectedHeaderParameters = this.decodeJWTHeaders(jwt)
    const { iss, kid } = this.getMandatoryHeadersOrFail(decodedJWT)
    const DID = await this.didService.getDIDDocumentFromDID(iss)
    const JWK = this.getJwkFromDid(DID, kid)
    const key = await this.didService.getPublicKeyFromJWK(JWK)
    const vpPayload = await this.decodeJWT(jwt, key)
    const verifiableCredentialsEnvelopes = vpPayload.payload['verifiableCredential']
    const verifiableCredentials = []
    if (!verifiableCredentialsEnvelopes) {
      throw new BadRequestException('Invalid content', 'The token should contain a VerifiablePresentation containing VerifiableCredentials')
    }
    if (Array.isArray(verifiableCredentialsEnvelopes)) {
      for (const element of verifiableCredentialsEnvelopes) {
        const vcDecoded = await this.decodeJWT(element?.id?.replace('data:application/vc+ld+json+jwt;', ''), key)
        verifiableCredentials.push(vcDecoded.payload)
      }
    } else {
      const vcDecoded = (await this.decodeJWT(verifiableCredentialsEnvelopes['id']?.replace('data:application/vc+ld+json+jwt;', ''), key)).payload
      verifiableCredentials.push(vcDecoded)
    }
    return { verifiableCredentials, key, JWK }
  }

  /**
   * Decodes JWT headers
   * @param jwtVerifiablePresentation a jwt containing a verifiable presentation as payload
   * @throws BadRequestException if the string is not a valid JWT
   * @throws InternalServerErrorException if the token was not decoded for an unknown reason
   * @private
   */
  private decodeJWTHeaders(jwtVerifiablePresentation: string) {
    try {
      return decodeProtectedHeader(jwtVerifiablePresentation)
    } catch (error) {
      if (error?.message === 'Invalid Token or Protected Header formatting') {
        throw new BadRequestException('Invalid payload', 'The payload is not a valid JWT and was not decoded')
      } else {
        throw new InternalServerErrorException(
          'An error has occurred',
          `An unexpected error has occurred on server-side and we don't know what happened. Please report it
${error.message}`
        )
      }
    }
  }

  /**
   * Retrieves the two mandatory headers iss & kid from a JWT headers. Throws a BadRequestException if one of them is not present
   * @param decodedHeaders
   * @private
   * @see https://www.w3.org/TR/vc-jose-cose/#using-header-params-claims-key-discovery
   */
  private getMandatoryHeadersOrFail(decodedHeaders: ProtectedHeaderParameters) {
    const iss = decodedHeaders['iss'] as string
    const kid = decodedHeaders['kid']
    this.checkIssIsPresent(iss)
    this.checkKidIsPresent(kid)
    return { iss, kid }
  }

  /**
   * checks whether the kid header passed as parameter is present
   * @param kid a jwt header, might be null, empty or undefined
   * @throws BadRequestException if the header is not filled
   * @private
   */
  private checkKidIsPresent(kid: string) {
    if (!kid) {
      throw new BadRequestException('Invalid request', 'The kid header referencing the verificationMethod of the DID is missing')
    }
  }

  /**
   * checks whether the iss header passed as parameter is present
   * @param iss a jwt header, might be null, empty or undefined
   * @throws BadRequestException if the header is not filled
   * @private
   */
  private checkIssIsPresent(iss) {
    if (!iss) {
      throw new BadRequestException('Invalid request', "The iss header referencing the issuer's DID is missing")
    }
  }

  /**
   * Retrieves the verificationMethod JWK from a DID based on the JWT's kid header
   * @param DID the DIDDocument
   * @param kid the verificationMethod name
   * @private
   */
  private getJwkFromDid(DID: DIDDocument, kid: string) {
    try {
      return this.didService.getJWKFromDID(DID, kid)
    } catch (error) {
      throw new BadRequestException('Invalid request', error.message)
    }
  }

  private async decodeJWT(jwt, key) {
    try {
      return await jwtVerify(jwt, key)
    } catch (error) {
      throw new BadRequestException('JWSSignatureVerificationFailed', 'The signature validation has failed')
    }
  }
}
