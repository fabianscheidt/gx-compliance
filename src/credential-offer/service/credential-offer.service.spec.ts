import { JsonWebKey } from 'did-resolver/src/resolver'
import { GenerateKeyPairResult, generateKeyPair } from 'jose'
import { Counter, Summary } from 'prom-client'

import { ComplianceCredentialDto, CredentialSubjectDto, VerifiableCredentialDto, VerifiablePresentationDto } from '../../common/dto'
import { ConformityLevelEnum } from '../../common/enum/conformity-level.enum'
import { DIDService } from '../../common/services/did.service'
import { EvidenceService } from '../../common/services/evidence.service'
import { JwtSignatureValidationService } from '../../jwt/service/jwt-signature-validation.service'
import { JwtSignatureService } from '../../jwt/service/jwt-signature.service'
import { JwtToVpService } from '../../jwt/service/jwt-to-vp.service'
import { RegistryService } from '../../vp-validation/service/registry.service'
import { VerifiablePresentationValidationService } from '../../vp-validation/service/verifiable-presentation-validation.service'
import { CredentialOfferService } from './credential-offer.service'

describe('CredentialOfferService', () => {
  const validVpJwt =
    'eyJhbGciOiJFUzI1NiIsInR5cCI6InZjK2xkK2pzb24rand0IiwiY3R5IjoidmMrbGQranNvbiIsImlzcyI6ImRpZDp3ZWI6Z2FpYS14LnVuaWNvcm4taG9tZS5jb20iLCJraWQiOiJkaWQ6d2ViOmdhaWEteC51bmljb3JuLWhvbWUuY29tI1g1MDktSldLMjAyMCJ9.eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvZXhhbXBsZXMvdjIiXSwidHlwZSI6IlZlcmlmaWFibGVQcmVzZW50YXRpb24iLCJ2ZXJpZmlhYmxlQ3JlZGVudGlhbCI6W3siQGNvbnRleHQiOiJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJpZCI6ImRhdGE6YXBwbGljYXRpb24vdmMrbGQranNvbitqd3Q7ZXlKaGJHY2lPaUpGVXpJMU5pSXNJblI1Y0NJNkluWmpLMnhrSzJwemIyNHJhbmQwSWl3aVkzUjVJam9pZG1NcmJHUXJhbk52YmlJc0ltbHpjeUk2SW1ScFpEcDNaV0k2WjJGcFlTMTRMblZ1YVdOdmNtNHRhRzl0WlM1amIyMGlMQ0pyYVdRaU9pSmthV1E2ZDJWaU9tZGhhV0V0ZUM1MWJtbGpiM0p1TFdodmJXVXVZMjl0STFnMU1Ea3RTbGRMTWpBeU1DSjkuZXlKQVkyOXVkR1Y0ZENJNld5Sm9kSFJ3Y3pvdkwzZDNkeTUzTXk1dmNtY3Zibk12WTNKbFpHVnVkR2xoYkhNdmRqSWlMQ0pvZEhSd2N6b3ZMM2N6YVdRdWIzSm5MMmRoYVdFdGVDOWtaWFpsYkc5d2JXVnVkQ01pWFN3aWFXUWlPaUpvZEhSd2N6b3ZMM1J5ZFhOMFpXUXRhWE56ZFdWeUxtTnZiUzl3WVhKMGFXTnBjR0Z1ZEM1cWMyOXVJaXdpZEhsd1pTSTZXeUpXWlhKcFptbGhZbXhsUTNKbFpHVnVkR2xoYkNKZExDSnBjM04xWlhJaU9pSmthV1E2ZDJWaU9tZGhhV0V0ZUM1MWJtbGpiM0p1TFdodmJXVXVZMjl0SWl3aWRtRnNhV1JHY205dElqb2lNakF5TkMwd09DMHdNbFF3T0RveE9EbzFNQzQ1TWpRck1EQTZNREFpTENKdVlXMWxJam9pUlhoaGJYQnNaU0JNZEdRZ1RHVm5ZV3hRWlhKemIyNGlMQ0pqY21Wa1pXNTBhV0ZzVTNWaWFtVmpkQ0k2ZXlKdVlXMWxJam9pUlhoaGJYQnNaU0JNZEdRaUxDSm9aV0ZrY1hWaGNuUmxjbk5CWkdSeVpYTnpJanA3SW5SNWNHVWlPaUpuZURwQlpHUnlaWE56SWl3aVkyOTFiblJ5ZVVOdlpHVWlPaUpHVWlJc0ltTnZkVzUwY25sT1lXMWxJam9pUm5KaGJtTmxJbjBzSW1kNE9uSmxaMmx6ZEhKaGRHbHZiazUxYldKbGNpSTZleUpwWkNJNkltaDBkSEJ6T2k4dmRISjFjM1JsWkMxcGMzTjFaWEl1WTI5dEwyeHliaTVxYzI5dUkyTnpJbjBzSW14bFoyRnNRV1JrY21WemN5STZleUowZVhCbElqb2laM2c2UVdSa2NtVnpjeUlzSW1OdmRXNTBjbmxEYjJSbElqb2lSbElpTENKamIzVnVkSEo1VG1GdFpTSTZJa1p5WVc1alpTSjlMQ0pwWkNJNkltaDBkSEJ6T2k4dmRISjFjM1JsWkMxcGMzTjFaWEl1WTI5dEwzQmhjblJwWTJsd1lXNTBMbXB6YjI0alkzTWlMQ0owZVhCbElqb2laM2c2VEdWbllXeFFaWEp6YjI0aWZYMC5Gbjd6WFF2UEwxUExOajNHck9RcXN5MVlLWmgxcFpVcHVwbFdscV9IQjNQZ3dITy04SGViTWYyc1FVWkVIbWsweXc1dmVkQk1nNUIzS29TazhmcVF2QSIsInR5cGUiOiJFbnZlbG9wZWRWZXJpZmlhYmxlQ3JlZGVudGlhbCJ9LHsiQGNvbnRleHQiOiJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJpZCI6ImRhdGE6YXBwbGljYXRpb24vdmMrbGQranNvbitqd3Q7ZXlKaGJHY2lPaUpGVXpJMU5pSXNJblI1Y0NJNkluWmpLMnhrSzJwemIyNHJhbmQwSWl3aVkzUjVJam9pZG1NcmJHUXJhbk52YmlJc0ltbHpjeUk2SW1ScFpEcDNaV0k2WjJGcFlTMTRMblZ1YVdOdmNtNHRhRzl0WlM1amIyMGlMQ0pyYVdRaU9pSmthV1E2ZDJWaU9tZGhhV0V0ZUM1MWJtbGpiM0p1TFdodmJXVXVZMjl0STFnMU1Ea3RTbGRMTWpBeU1DSjkuZXlKQVkyOXVkR1Y0ZENJNld5Sm9kSFJ3Y3pvdkwzZDNkeTUzTXk1dmNtY3Zibk12WTNKbFpHVnVkR2xoYkhNdmRqSWlMQ0pvZEhSd2N6b3ZMM2N6YVdRdWIzSm5MMmRoYVdFdGVDOWtaWFpsYkc5d2JXVnVkQ01pWFN3aWRIbHdaU0k2V3lKV1pYSnBabWxoWW14bFEzSmxaR1Z1ZEdsaGJDSmRMQ0oyWVd4cFpFWnliMjBpT2lJeU1ESTBMVEE0TFRBeVZEQTRPakU0T2pVd0xqa3lOeXN3TURvd01DSXNJbWxrSWpvaWFIUjBjSE02THk5MGNuVnpkR1ZrTFdsemMzVmxjaTVqYjIwdmFYTnpkV1Z5TG1wemIyNGlMQ0pwYzNOMVpYSWlPaUprYVdRNmQyVmlPbWRoYVdFdGVDNTFibWxqYjNKdUxXaHZiV1V1WTI5dElpd2lZM0psWkdWdWRHbGhiRk4xWW1wbFkzUWlPbnNpYVdRaU9pSm9kSFJ3Y3pvdkwzUnlkWE4wWldRdGFYTnpkV1Z5TG1OdmJTOXBjM04xWlhJdWFuTnZiaU5qY3lJc0luUjVjR1VpT2lKbmVEcEpjM04xWlhJaUxDSm5ZV2xoZUZSbGNtMXpRVzVrUTI5dVpHbDBhVzl1Y3lJNklqUmlaRGMxTlRRd09UYzBORFJqT1RZd01qa3lZalEzTWpaak1tVm1ZVEV6TnpNME9EVmxPR0UxTlRZMVpEazBaRFF4TVRrMU1qRTBZelZsTUdObFlqTWlmWDAuWDY0NU1Wc3NGdGNpQjJnMkptQzNXdWZJMFlfTklaeDE0TkdLaWdyN2JWQmdvS3FpNlJKQ0Z3bWZiZU1BWWw5eV9LNVpuMlJwamZtX09kTFNORndHenciLCJ0eXBlIjoiRW52ZWxvcGVkVmVyaWZpYWJsZUNyZWRlbnRpYWwifSx7IkBjb250ZXh0IjoiaHR0cHM6Ly93d3cudzMub3JnL25zL2NyZWRlbnRpYWxzL3YyIiwiaWQiOiJkYXRhOmFwcGxpY2F0aW9uL3ZjK2xkK2pzb24rand0O2V5SmhiR2NpT2lKRlV6STFOaUlzSW5SNWNDSTZJblpqSzJ4a0sycHpiMjRyYW5kMElpd2lZM1I1SWpvaWRtTXJiR1FyYW5OdmJpSXNJbWx6Y3lJNkltUnBaRHAzWldJNloyRnBZUzE0TG5WdWFXTnZjbTR0YUc5dFpTNWpiMjBpTENKcmFXUWlPaUprYVdRNmQyVmlPbWRoYVdFdGVDNTFibWxqYjNKdUxXaHZiV1V1WTI5dEkxZzFNRGt0U2xkTE1qQXlNQ0o5LmV5SkFZMjl1ZEdWNGRDSTZXeUpvZEhSd2N6b3ZMM2QzZHk1M015NXZjbWN2Ym5NdlkzSmxaR1Z1ZEdsaGJITXZkaklpTENKb2RIUndjem92TDNjemFXUXViM0puTDJkaGFXRXRlQzlrWlhabGJHOXdiV1Z1ZENNaVhTd2lkSGx3WlNJNld5SldaWEpwWm1saFlteGxRM0psWkdWdWRHbGhiQ0pkTENKcFpDSTZJbWgwZEhCek9pOHZkSEoxYzNSbFpDMXBjM04xWlhJdVkyOXRMMnh5Ymk1cWMyOXVJaXdpYVhOemRXVnlJam9pWkdsa09uZGxZanBuWVdsaExYZ3VkVzVwWTI5eWJpMW9iMjFsTG1OdmJTSXNJblpoYkdsa1JuSnZiU0k2SWpJd01qUXRNRGd0TURKVU1EZzZNVGc2TlRBdU9USTRLekF3T2pBd0lpd2lZM0psWkdWdWRHbGhiRk4xWW1wbFkzUWlPbnNpYVdRaU9pSm9kSFJ3Y3pvdkwzUnlkWE4wWldRdGFYTnpkV1Z5TG1OdmJTOXNjbTR1YW5OdmJpTmpjeUlzSW5SNWNHVWlPaUpuZURwV1lYUkpSQ0lzSW1kNE9uWmhkRWxFSWpvaVFrVXdOell5TnpRM056SXhJaXdpWTI5MWJuUnllVU52WkdVaU9pSkNSU0o5TENKbGRtbGtaVzVqWlNJNlczc2laM2c2WlhacFpHVnVZMlZWVWt3aU9pSm9kSFJ3T2k4dlpXTXVaWFZ5YjNCaExtVjFMM1JoZUdGMGFXOXVYMk4xYzNSdmJYTXZkbWxsY3k5elpYSjJhV05sY3k5amFHVmphMVpoZEZObGNuWnBZMlVpTENKbmVEcGxlR1ZqZFhScGIyNUVZWFJsSWpvaU1qQXlOQzB3TlMweE5WUXhNam94TURveU15NDVNREJhSWl3aVozZzZaWFpwWkdWdVkyVlBaaUk2SW1kNE9uWmhkRWxFSW4xZGZRLmE0aU8zVnBGeUlYeGtNZGZzb0JsODhwM0hXNG9ZeXFTLXNDYzJxN0UxYUp3Tm04TXNib3RpazlTZ3dDQjZFS2E4dFM3Q3g3U2VmY3drWmYxSGhfQ29BIiwidHlwZSI6IkVudmVsb3BlZFZlcmlmaWFibGVDcmVkZW50aWFsIn0seyJAY29udGV4dCI6Imh0dHBzOi8vd3d3LnczLm9yZy9ucy9jcmVkZW50aWFscy92MiIsImlkIjoiZGF0YTphcHBsaWNhdGlvbi92YytsZCtqc29uK2p3dDtleUpoYkdjaU9pSkZVekkxTmlJc0luUjVjQ0k2SW5aaksyeGtLMnB6YjI0cmFuZDBJaXdpWTNSNUlqb2lkbU1yYkdRcmFuTnZiaUlzSW1semN5STZJbVJwWkRwM1pXSTZaMkZwWVMxNExuVnVhV052Y200dGFHOXRaUzVqYjIwaUxDSnJhV1FpT2lKa2FXUTZkMlZpT21kaGFXRXRlQzUxYm1samIzSnVMV2h2YldVdVkyOXRJMWcxTURrdFNsZExNakF5TUNKOS5leUpBWTI5dWRHVjRkQ0k2V3lKb2RIUndjem92TDNkM2R5NTNNeTV2Y21jdmJuTXZZM0psWkdWdWRHbGhiSE12ZGpJaUxDSm9kSFJ3Y3pvdkwzY3phV1F1YjNKbkwyZGhhV0V0ZUM5a1pYWmxiRzl3YldWdWRDTWlYU3dpYVdRaU9pSm9kSFJ3Y3pvdkwzUnlkWE4wWldRdGFYTnpkV1Z5TG1OdmJTOXpieTVxYzI5dUlpd2lkSGx3WlNJNld5SldaWEpwWm1saFlteGxRM0psWkdWdWRHbGhiQ0pkTENKcGMzTjFaWElpT2lKa2FXUTZkMlZpT21kaGFXRXRlQzUxYm1samIzSnVMV2h2YldVdVkyOXRJaXdpZG1Gc2FXUkdjbTl0SWpvaU1qQXlOQzB3T0Mwd01sUXdPRG94T0RvMU1DNDVNekFyTURBNk1EQWlMQ0p1WVcxbElqb2lSWGhoYlhCc1pTQk1kR1FnVTJWeWRtbGpaVTltWm1WeWFXNW5JaXdpWTNKbFpHVnVkR2xoYkZOMVltcGxZM1FpT25zaWFXUWlPaUpvZEhSd2N6b3ZMM1J5ZFhOMFpXUXRhWE56ZFdWeUxtTnZiUzl6Ynk1cWMyOXVJMk56SWl3aWRIbHdaU0k2SW1kNE9sTmxjblpwWTJWUFptWmxjbWx1WnlJc0luQnliM1pwWkdWa1Fua2lPaUpvZEhSd2N6b3ZMM1J5ZFhOMFpXUXRhWE56ZFdWeUxtTnZiUzl3WVhKMGFXTnBjR0Z1ZEM1cWMyOXVJMk56SWl3aWMyVnlkbWxqWlU5bVptVnlhVzVuVkdWeWJYTkJibVJEYjI1a2FYUnBiMjV6SWpwN0luUjVjR1VpT2lKVVpYSnRjMEZ1WkVOdmJtUnBkR2x2Ym5NaUxDSjFjbXdpT25zaWRIbHdaU0k2SW5oelpEcGhibmxWVWtraUxDSkFkbUZzZFdVaU9pSm9kSFJ3Y3pvdkwyUnZiV0ZwYmk1amIyMHZkSE5oYm1SamN5SjlMQ0pvWVhOb0lqb2ljMmhoTWpVMkxYTnpZWE5oVTBGellTSjlMQ0puZURwc1pXZGhiR3g1UW1sdVpHbHVaMEZqZEhNaU9sdDdJblI1Y0dVaU9pSm5lRHBNWldkaGJFUnZZM1Z0Wlc1MElpd2lkWEpzSWpvaWFIUjBjSE02THk5MGNuVnpkR1ZrTFdsemMzVmxjaTVqYjIwdmMyVnlkbWxqWlMxdlptWmxjbWx1Wnk5c1pXZGhiR3g1TFdKcGJtUnBibWN0WVdOMExuQmtaaUlzSW0xcGJXVlVlWEJsY3lJNld5SmhjSEJzYVdOaGRHbHZiaTl3WkdZaVhYMHNleUowZVhCbElqb2laM2c2VEdWbllXeEViMk4xYldWdWRDSXNJblZ5YkNJNkltaDBkSEJ6T2k4dmRISjFjM1JsWkMxcGMzTjFaWEl1WTI5dEwzTmxjblpwWTJVdGIyWm1aWEpwYm1jdmJHVm5ZV3hzZVMxaWFXNWthVzVuTFdGamRDMHlMbkJrWmlJc0ltMXBiV1ZVZVhCbGN5STZXeUpoY0hCc2FXTmhkR2x2Ymk5d1pHWWlYWDFkTENKelpYSjJhV05sVUc5c2FXTjVJam9pWVd4c2IzY2lMQ0prWVhSaFFXTmpiM1Z1ZEVWNGNHOXlkQ0k2ZXlKMGVYQmxJam9pUkdGMFlVRmpZMjkxYm5SRmVIQnZjblFpTENKeVpYRjFaWE4wVkhsd1pTSTZJa0ZRU1NJc0ltRmpZMlZ6YzFSNWNHVWlPaUprYVdkcGRHRnNJaXdpWm05eWJXRjBWSGx3WlNJNkltcHpiMjRpZlN3aVlXZG5jbVZuWVhScGIyNVBabEpsYzI5MWNtTmxjeUk2V3lKb2RIUndjem92TDNSeWRYTjBaV1F0YVhOemRXVnlMbU52YlM5eVpYTnZkWEpqWlM1cWMyOXVJMk56SWl3aWFIUjBjSE02THk5MGNuVnpkR1ZrTFdsemMzVmxjaTVqYjIwdlpHRjBZUzVxYzI5dUkyTnpJbDE5ZlEuVWVGazBMc2JCU1hrems2Z1J3SkJGb2pDOG1JVDVPYVZzVk5sUUpTRmpRQjhPX2VtYlBFdEdBLUZIeWVpRmt4ekxucmh0Mkp2Z0tzOWRzckFMb3drc3ciLCJ0eXBlIjoiRW52ZWxvcGVkVmVyaWZpYWJsZUNyZWRlbnRpYWwifSx7IkBjb250ZXh0IjoiaHR0cHM6Ly93d3cudzMub3JnL25zL2NyZWRlbnRpYWxzL3YyIiwiaWQiOiJkYXRhOmFwcGxpY2F0aW9uL3ZjK2xkK2pzb24rand0O2V5SmhiR2NpT2lKRlV6STFOaUlzSW5SNWNDSTZJblpqSzJ4a0sycHpiMjRyYW5kMElpd2lZM1I1SWpvaWRtTXJiR1FyYW5OdmJpSXNJbWx6Y3lJNkltUnBaRHAzWldJNloyRnBZUzE0TG5WdWFXTnZjbTR0YUc5dFpTNWpiMjBpTENKcmFXUWlPaUprYVdRNmQyVmlPbWRoYVdFdGVDNTFibWxqYjNKdUxXaHZiV1V1WTI5dEkxZzFNRGt0U2xkTE1qQXlNQ0o5LmV5SkFZMjl1ZEdWNGRDSTZXeUpvZEhSd2N6b3ZMM2QzZHk1M015NXZjbWN2Ym5NdlkzSmxaR1Z1ZEdsaGJITXZkaklpTENKb2RIUndjem92TDNjemFXUXViM0puTDJkaGFXRXRlQzlrWlhabGJHOXdiV1Z1ZENNaVhTd2lhV1FpT2lKb2RIUndjem92TDNSeWRYTjBaV1F0YVhOemRXVnlMbU52YlM5eVpYTnZkWEpqWlM1cWMyOXVJaXdpZEhsd1pTSTZXeUpXWlhKcFptbGhZbXhsUTNKbFpHVnVkR2xoYkNKZExDSnBjM04xWlhJaU9pSmthV1E2ZDJWaU9tZGhhV0V0ZUM1MWJtbGpiM0p1TFdodmJXVXVZMjl0SWl3aWRtRnNhV1JHY205dElqb2lNakF5TkMwd09DMHdNbFF3T0RveE9EbzFNQzQ1TXpJck1EQTZNREFpTENKdVlXMWxJam9pUlhoaGJYQnNaU0JNZEdRZ1VHaDVjMmxqWVd4U1pYTnZkWEpqWlNJc0ltTnlaV1JsYm5ScFlXeFRkV0pxWldOMElqcDdJbWxrSWpvaWFIUjBjSE02THk5MGNuVnpkR1ZrTFdsemMzVmxjaTVqYjIwdmNtVnpiM1Z5WTJVdWFuTnZiaU5qY3lJc0luUjVjR1VpT2lKbmVEcFFhSGx6YVdOaGJGSmxjMjkxY21ObElpd2liV0ZwYm5SaGFXNWxaRUo1SWpvaWFIUjBjSE02THk5MGNuVnpkR1ZrTFdsemMzVmxjaTVqYjIwdmNHRnlkR2xqYVhCaGJuUXVhbk52YmlOamN5SXNJbXhwWTJWdWMyVWlPaUpGVUV3dE1pNHdJaXdpY21WemIzVnlZMlZRYjJ4cFkza2lPaUpoYkd4dmR5SXNJbXh2WTJGMGFXOXVJanA3SWtCMGVYQmxJam9pUVdSa2NtVnpjeUlzSW1OdmRXNTBjbmxEYjJSbElqb2lRa1VpZlgxOS5hS3RsbFR5RUZGcm1KbzVSMk5NTllFbXdwUWYzRFRQc2RteldBdVZySXp6QldjNFZNTTZNTDhZSm1lREw5UGxxTXpTQlBXVVBuekpaZFd3QlUtaDlDZyIsInR5cGUiOiJFbnZlbG9wZWRWZXJpZmlhYmxlQ3JlZGVudGlhbCJ9LHsiQGNvbnRleHQiOiJodHRwczovL3d3dy53My5vcmcvbnMvY3JlZGVudGlhbHMvdjIiLCJpZCI6ImRhdGE6YXBwbGljYXRpb24vdmMrbGQranNvbitqd3Q7ZXlKaGJHY2lPaUpGVXpJMU5pSXNJblI1Y0NJNkluWmpLMnhrSzJwemIyNHJhbmQwSWl3aVkzUjVJam9pZG1NcmJHUXJhbk52YmlJc0ltbHpjeUk2SW1ScFpEcDNaV0k2WjJGcFlTMTRMblZ1YVdOdmNtNHRhRzl0WlM1amIyMGlMQ0pyYVdRaU9pSmthV1E2ZDJWaU9tZGhhV0V0ZUM1MWJtbGpiM0p1TFdodmJXVXVZMjl0STFnMU1Ea3RTbGRMTWpBeU1DSjkuZXlKQVkyOXVkR1Y0ZENJNld5Sm9kSFJ3Y3pvdkwzZDNkeTUzTXk1dmNtY3Zibk12WTNKbFpHVnVkR2xoYkhNdmRqSWlMQ0pvZEhSd2N6b3ZMM2N6YVdRdWIzSm5MMmRoYVdFdGVDOWtaWFpsYkc5d2JXVnVkQ01pWFN3aWFXUWlPaUpvZEhSd2N6b3ZMM1J5ZFhOMFpXUXRhWE56ZFdWeUxtTnZiUzlrWVhSaExtcHpiMjRpTENKMGVYQmxJanBiSWxabGNtbG1hV0ZpYkdWRGNtVmtaVzUwYVdGc0lsMHNJbWx6YzNWbGNpSTZJbVJwWkRwM1pXSTZaMkZwWVMxNExuVnVhV052Y200dGFHOXRaUzVqYjIwaUxDSjJZV3hwWkVaeWIyMGlPaUl5TURJMExUQTRMVEF5VkRBNE9qRTRPalV3TGprek15c3dNRG93TUNJc0ltNWhiV1VpT2lKRmVHRnRjR3hsSUV4MFpDQkVZWFJoVW1WemIzVnlZMlVpTENKamNtVmtaVzUwYVdGc1UzVmlhbVZqZENJNmV5SnBaQ0k2SW1oMGRIQnpPaTh2ZEhKMWMzUmxaQzFwYzNOMVpYSXVZMjl0TDJSaGRHRXVhbk52YmlOamN5SXNJblI1Y0dVaU9pSm5lRHBFWVhSaFVtVnpiM1Z5WTJVaUxDSndjbTlrZFdObFpFSjVJam9pYUhSMGNITTZMeTkwY25WemRHVmtMV2x6YzNWbGNpNWpiMjB2Y0dGeWRHbGphWEJoYm5RdWFuTnZiaU5qY3lJc0ltTnZjSGx5YVdkb2RFOTNibVZrUW5raU9pSm9kSFJ3Y3pvdkwzUnlkWE4wWldRdGFYTnpkV1Z5TG1OdmJTOXdZWEowYVdOcGNHRnVkQzVxYzI5dUkyTnpJaXdpWTI5dWRHRnBibk5RU1VraU9uUnlkV1VzSW14cFkyVnVjMlVpT2lKRlVFd3RNaTR3SWl3aWNtVnpiM1Z5WTJWUWIyeHBZM2tpT2lKaGJHeHZkeUlzSW1WNGNHOXpaV1JVYUhKdmRXZG9JanA3SWtCMGVYQmxJam9pUkdGMFlVVjRZMmhoYm1kbFEyOXRjRzl1Wlc1MEluMHNJbU52Ym5ObGJuUWlPbnNpUUhSNWNHVWlPaUpEYjI1elpXNTBJaXdpYkdWbllXeENZWE5wY3lJNklrZEVVRkl0Tmk0eElpd2ljSFZ5Y0c5elpTSTZJbTE1VUhWeWNHOXpaU0lzSW1SaGRHRlFjbTkwWldOMGFXOXVRMjl1ZEdGamRGQnZhVzUwSWpvaVozQmtja0IwY25WemRHVmtMV2x6YzNWbGNpNWpiMjBpTENKamIyNXpaVzUwVjJsMGFHUnlZWGRoYkVOdmJuUmhZM1JRYjJsdWRDSTZJbWR3WkhKQWRISjFjM1JsWkMxcGMzTjFaWEl1WTI5dEluMTlmUS53Y19ZUXp3S05oWUgyaVIxOTZrQVpUc1pNQUpLc2xhWHM3Z0tQdmotYWd6LUw4ZlRzRTlTYlVTTWhZQWRjM20yU3h6ajFPTjh1NTEwTkhObzY0RFFtUSIsInR5cGUiOiJFbnZlbG9wZWRWZXJpZmlhYmxlQ3JlZGVudGlhbCJ9XSwiaXNzdWVyIjoiZGlkOndlYjpnYWlhLXgudW5pY29ybi1ob21lLmNvbSIsInZhbGlkRnJvbSI6IjIwMjQtMDgtMDJUMDg6MTg6NTAuOTU3KzAwOjAwIn0.2gOFJDpmhZYYQt9g2qTdkRxkVv1uzQGmtj_e6Od1WH0-4KbPNK5k_lSumQvF1JvhBzzOE1yAJVz2L_PH1bwbvg'

  const didServiceMock: DIDService = {
    checkx5uMatchesPublicKey: jest.fn(),
    checkx5cMatchesPublicKey: jest.fn()
  } as unknown as DIDService

  const jwtSignatureValidationServiceMock: JwtSignatureValidationService = {
    validateJWTAndConvertToVCs: jest.fn()
  } as unknown as JwtSignatureValidationService

  const jwtToVPServiceMock: JwtToVpService = {
    JWTVcsToVP: jest.fn()
  } as unknown as JwtToVpService

  const verifiablePresentationValidationServiceMock: VerifiablePresentationValidationService = {
    validate: jest.fn()
  } as unknown as VerifiablePresentationValidationService

  const evidenceServiceMock: EvidenceService = {
    createComplianceCredential: jest.fn()
  } as unknown as EvidenceService

  const jwtSignatureServiceMock: JwtSignatureService = {
    signVerifiableCredential: jest.fn()
  } as unknown as JwtSignatureService

  const registryServiceMock: RegistryService = {
    isValidCertificateChain: jest.fn()
  } as unknown as RegistryService

  const counterMock: Counter = {
    inc: jest.fn()
  } as unknown as Counter
  const summaryMock: Summary = {
    startTimer: () => {
      return jest.fn()
    }
  } as unknown as Summary

  const verifiableCredentialsMock: any[] = [{ id: 'test-credential-1' }, { id: 'test-credential-2' }]

  let service: CredentialOfferService
  let keyPair: GenerateKeyPairResult
  let verifiablePresentationMock: VerifiablePresentationDto<VerifiableCredentialDto<CredentialSubjectDto>>
  let complianceCredentialMock: VerifiableCredentialDto<ComplianceCredentialDto>
  beforeAll(async () => {
    service = new CredentialOfferService(
      didServiceMock,
      evidenceServiceMock,
      jwtSignatureServiceMock,
      jwtSignatureValidationServiceMock,
      jwtToVPServiceMock,
      registryServiceMock,
      verifiablePresentationValidationServiceMock,
      counterMock,
      counterMock,
      summaryMock
    )

    keyPair = await generateKeyPair('PS256')
  })
  beforeEach(async () => {
    verifiablePresentationMock = {
      '@context': undefined,
      '@type': ['VerifiablePresentation'],
      verifiableCredential: []
    }
    jest.mocked(jwtToVPServiceMock.JWTVcsToVP).mockReturnValue(verifiablePresentationMock)
    jest.mocked(jwtSignatureValidationServiceMock.validateJWTAndConvertToVCs).mockResolvedValue({
      verifiableCredentials: verifiableCredentialsMock,
      key: keyPair.publicKey,
      JWK: {
        x5u: 'certificateUrl'
      } as unknown as JsonWebKey
    })

    complianceCredentialMock = {
      '@context': undefined,
      credentialSubject: undefined,
      issuer: '',
      type: 'ComplianceCredential',
      validFrom: ''
    }
    jest.mocked(evidenceServiceMock.createComplianceCredential).mockResolvedValue(complianceCredentialMock)

    jest.mocked(jwtSignatureServiceMock.signVerifiableCredential).mockResolvedValue('jwt-compliance-credential')

    jest.mocked(verifiablePresentationValidationServiceMock.validate).mockResolvedValue({
      conforms: true,
      results: []
    })
    jest.mocked(registryServiceMock.isValidCertificateChain).mockResolvedValue(true)
    jest.mocked(didServiceMock.checkx5cMatchesPublicKey).mockResolvedValue({ conforms: true, cert: 'cert' })
    jest.mocked(didServiceMock.checkx5uMatchesPublicKey).mockResolvedValue({ conforms: true, cert: 'cert' })
  })

  afterEach(() => {
    jest.resetAllMocks()
  })

  it('should throw a conflict exception when the DID contains neither x5c nor x5u', async () => {
    // Given the DID does not contain x5c nor x5u
    jest.mocked(jwtSignatureValidationServiceMock.validateJWTAndConvertToVCs).mockResolvedValue({
      verifiableCredentials: verifiableCredentialsMock,
      key: keyPair.publicKey,
      JWK: {} as unknown as JsonWebKey
    })

    // When I check the VC for compliance
    // Then it's rejected and I get the message that x5c and x5u are missing
    await expect(
      service.checkAndIssueComplianceCredential(validVpJwt, 'https://example.org/verifiable-credential/123', ConformityLevelEnum.BASIC_CONFORMITY)
    ).rejects.toThrow('DID does not contain x5u nor x5c')
  })

  it('should check VC-JWT and issue compliance credential when the did contains a x5u', async () => {
    const result: string = await service.checkAndIssueComplianceCredential(
      validVpJwt,
      'https://example.org/verifiable-credential/123',
      ConformityLevelEnum.BASIC_CONFORMITY
    )

    expect(result).toEqual('jwt-compliance-credential')
    expect(evidenceServiceMock.createComplianceCredential).toHaveBeenCalledWith(
      verifiablePresentationMock,
      'https://example.org/verifiable-credential/123'
    )
    expect(jwtSignatureServiceMock.signVerifiableCredential).toHaveBeenCalledWith(complianceCredentialMock)
    expect(didServiceMock.checkx5uMatchesPublicKey).toHaveBeenCalledWith(keyPair.publicKey, 'certificateUrl')
    expect(jwtSignatureValidationServiceMock.validateJWTAndConvertToVCs).toHaveBeenCalledWith(validVpJwt)
    expect(verifiablePresentationValidationServiceMock.validate).toHaveBeenCalledWith(
      verifiablePresentationMock,
      ConformityLevelEnum.BASIC_CONFORMITY
    )
  })

  it('should check VC-JWT and issue compliance credential when the did contains a x5c', async () => {
    // Given the DID contains x5c
    jest.mocked(jwtSignatureValidationServiceMock.validateJWTAndConvertToVCs).mockResolvedValue({
      verifiableCredentials: verifiableCredentialsMock,
      key: keyPair.publicKey,
      JWK: {
        x5c: ['leaf', 'root']
      } as unknown as JsonWebKey
    })
    // When I check the VC for compliance
    const result: string = await service.checkAndIssueComplianceCredential(
      validVpJwt,
      'https://example.org/verifiable-credential/123',
      ConformityLevelEnum.BASIC_CONFORMITY
    )
    // Then it's validated and I get a compliance credential
    expect(result).toEqual('jwt-compliance-credential')
    expect(evidenceServiceMock.createComplianceCredential).toHaveBeenCalledWith(
      verifiablePresentationMock,
      'https://example.org/verifiable-credential/123'
    )
    expect(jwtSignatureServiceMock.signVerifiableCredential).toHaveBeenCalledWith(complianceCredentialMock)
    expect(didServiceMock.checkx5cMatchesPublicKey).toHaveBeenCalledWith(keyPair.publicKey, ['leaf', 'root'])
    expect(jwtSignatureValidationServiceMock.validateJWTAndConvertToVCs).toHaveBeenCalledWith(validVpJwt)
    expect(verifiablePresentationValidationServiceMock.validate).toHaveBeenCalledWith(
      verifiablePresentationMock,
      ConformityLevelEnum.BASIC_CONFORMITY
    )
  })

  it('should throw an exception if the verifiable presentation is not compliant', () => {
    jest.mocked(verifiablePresentationValidationServiceMock.validate).mockResolvedValue({
      conforms: false,
      results: []
    })

    expect(
      service.checkAndIssueComplianceCredential(validVpJwt, 'https://example.org/verifiable-credential/123', ConformityLevelEnum.BASIC_CONFORMITY)
    ).rejects.toThrow('Unable to validate compliance')

    expect(jwtSignatureServiceMock.signVerifiableCredential).not.toHaveBeenCalled()
  })

  it('should throw an exception if the certificate is not matching the JWK', () => {
    jest.mocked(didServiceMock.checkx5uMatchesPublicKey).mockResolvedValue({ conforms: false, cert: 'cert' })
    expect(
      service.checkAndIssueComplianceCredential(validVpJwt, 'https://example.org/verifiable-credential/123', ConformityLevelEnum.BASIC_CONFORMITY)
    ).rejects.toThrow('Invalid DID')
  })
  it('should throw an exception if the certificate is not trusted by the registry', () => {
    jest.mocked(registryServiceMock.isValidCertificateChain).mockResolvedValue(false)
    expect(
      service.checkAndIssueComplianceCredential(validVpJwt, 'https://example.org/verifiable-credential/123', ConformityLevelEnum.BASIC_CONFORMITY)
    ).rejects.toThrow('Invalid certificate')
  })
})
