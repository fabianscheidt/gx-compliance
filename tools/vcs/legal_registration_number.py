from datetime import datetime


def legal_registration_number(domain: str):
    return {
        "@context": [
            "https://www.w3.org/ns/credentials/v2",
            "https://w3id.org/gaia-x/development#"
        ],
        "type": [
            "VerifiableCredential",
            "VatID"
        ],
        "id": domain + "lrn.json",
        "issuer": "did:web:registration.lab.gaia-x.eu:v1",
        "validFrom": datetime.now().isoformat(),
        "credentialSubject": {
            "id": domain + "lrn.json#cs",
            "gx:vatID": "BE0762747721",
            "countryCode": "BE"
        },
        "evidence": [
            {
                "gx:evidenceURL": "http://ec.europa.eu/taxation_customs/vies/services/checkVatService",
                "gx:executionDate": "2024-05-15T12:10:23.900Z",
                "gx:evidenceOf": "gx:vatID"
            }
        ]
    }
