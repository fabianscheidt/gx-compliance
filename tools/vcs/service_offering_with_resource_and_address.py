from datetime import datetime


def service_offering_pointing_on_physical_resource(resource_name: str, domain: str, did_value: str):
    return {
        "@context": [
            "https://www.w3.org/ns/credentials/v2",
            "https://w3id.org/gaia-x/development#",
            {"xsd": "http://www.w3.org/2001/XMLSchema#"}
        ],
        "id": domain + "so.json",
        "type": [
            "VerifiableCredential",
            "ServiceOffering"
        ],
        "issuer": did_value,
        "validFrom": datetime.now().isoformat(),
        "name": "Example ltd ServiceOffering",
        "credentialSubject": {
            "id": domain + "so.json#cs",
            "providedBy": domain + "participant.json#cs",
            "serviceOfferingTermsAndConditions": {
                "type": "TermsAndConditions",
                "url": {
                    "type": "xsd:anyURI",
                    "@value": "https://domain.com/tsandcs",
                },
                "hash": "sha256-ssasaSAsa"
            },
            "servicePolicy": "allow",
            "dataAccountExport": {
                "type": "DataAccountExport",
                "requestType": "API",
                "accessType": "digital",
                "formatType": "json"
            },
            "aggregationOfResources": [
                domain + resource_name + ".json#cs"
            ]
        }
    }


def physical_resource_with_address(resource_name: str, domain: str, did_value: str):
    return {
        "@context": [
            "https://www.w3.org/ns/credentials/v2",
            "https://w3id.org/gaia-x/development#",
            {
                "xsd": "http://www.w3.org/2001/XMLSchema#"
            }
        ],
        "id": domain + resource_name + ".json",
        "type": [
            "VerifiableCredential",
            "PhysicalResource"
        ],
        "issuer": did_value,
        "validFrom": datetime.now().isoformat(),
        "name": "Example Ltd PhysicalResource",
        "credentialSubject": {
            "id": domain + resource_name + ".json#cs",
            "maintainedBy": domain + "participant.json#cs",
            "location": {
                "@type": "Address",
                "countryCode": "BE"
            }
        }
    }
